// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7

import { Injectable, OnInit } from "@angular/core";
import { App, ToastController } from "ionic-angular";
import { HttpClient } from "@angular/common/http";

import { AlertService } from "./alert.service";
import { ParamService } from "./param.service";




export class Walkassit {
  timestamp: any;
  status: number; // Disabled 0 Enabled 1 Error 2 Active 3
  person: Person; // actual position x,y of the person
  origin: Origin; // position x,y of the person when she start walking with the robot
  detected: boolean; // to know if the robot see the person or not
  activationDistance:number;
  targetAngularSpeed:number;
  barsAngle:number;
}

export class Person {
  x: number;
  y: number;
}

export class Origin {
  x: number;
  y: number;
}

export class Docking {
  status: number;
  detected: boolean;
}
/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/

export class Battery {
  autonomy: number; //the estimated remaining operation time, in minutes.
  current: number; //the actual current drawn from the battery, in Amps.
  remaining: number; //the percentage of energy remaining in the battery, range 0 - 100 %.
  status: number; // Charging 0 Chrged 1 Ok 2 Critical 3
  timestamp: number;
  voltage: number; // the actual battery voltage in Volts.
}

export class Anticollision {
  timestamp: number;
  enabled: boolean;
  locked: boolean;
  forward: number; // 0 ok , 1 speed low , 2 obstacle
  reverse: number;
  left: number;
  right: number;
}

export class Statistics {
  timestamp: number;
  totalTime: number;
  totalDistance: number;
}

export class Iostate {
  // receive robot's boutons input
  timestamp: number;
  dIn: any;
  aIn: any;
}

export class Navigation {
  avoided: number;
  status: number;
}

export class Localization {
  positionx: number;
  positiony: number;
  positiont: number;
}

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.

export class Differential {
  status: number;
}

@Injectable()
export class ApiService implements OnInit {
  batteryremaining: number;


  battery_status: Battery;
  battery_socket: WebSocket;
  hour: any;
  differential_status: Differential;
  differential_socket: WebSocket;

  navigation_status: Navigation;
  navigation_socket: WebSocket;

  statistics_socket: WebSocket;
  statistics_status: Statistics;

  anticollision_status: Anticollision;
  anticollision_socket: WebSocket;

  walker_socket: WebSocket;
  walker_states: Walkassit;

  docking_socket: WebSocket;
  docking_status: Docking;

  io_socket: WebSocket;
  io_status: Iostate;

  localization_status: Localization;
  localization_socket: WebSocket;

  statusRobot: number;
  cpt_jetsonOK: number;
  wifiok: boolean;
  metercovered: number;
  traveldebut: number;
  towardDocking: boolean;
  inUse: boolean;
  b_pressed: boolean;
  btnPush: boolean;
  is_connected: boolean;
  close_app: boolean;
  appStart: boolean;
  appOpened: boolean;
  batteryState: number;
  robotok: boolean;
  position_state:number=0;//0 ?  1 bad 2 good
  selectSpeed: any = 0.5;
  selectWalkZone:any=0.55;
  volumeState: number; //0 mute,  1 low,  2 hight
  httpskomnav: string;
  wsskomnav: string;
  httpsros: string;
  wssros: string;

  constructor(
    public toastCtrl: ToastController,
    public app: App,
    private httpClient: HttpClient,
    public alert: AlertService,
    public param: ParamService
  ) {
    this.is_connected = false; // to know if we are connected to the robot
    this.towardDocking = false; //to know if the robot is moving toward the docking

    this.traveldebut = 0;
    this.statusRobot = 10;
    this.metercovered = 0;
    this.cpt_jetsonOK = 0;
    this.appStart = false;
    this.close_app = false;
    this.appOpened = false;
    this.inUse = false; // to know if the enslavement of the legs is active or not
    this.robotok = false;
  }

  instanciate() {
    var apiserv = this;
    ////////////////////// localization socket

    this.localization_status = new Localization();

    this.localization_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/localization/socket",
      "json"
    );

    this.localization_socket.onerror = function (event) {
      console.log("error localization socket");
    };

    this.localization_socket.onopen = function (event) {
      console.log("localization socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.localization_status.positionx = test["Pose"]["X"];
        apiserv.localization_status.positiony = test["Pose"]["Y"];
        apiserv.localization_status.positiont = test["Pose"]["T"];
      };

      this.onclose = function () {
        console.log("localization socket closed");
      };
    };

    ////////////////////// differential socket

    this.differential_status = new Differential();

    this.differential_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/differential/socket",
      "json"
    );

    this.differential_socket.onerror = function (event) {
      console.log("error differential socket");
    };

    this.differential_socket.onopen = function (event) {
      console.log("differential socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.differential_status.status = test["Status"];
      };

      this.onclose = function () {
        console.log("differential socket closed");
      };
    };

    ////////////////////// navigation socket

    this.navigation_status = new Navigation();

    this.navigation_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/navigation/socket",
      "json"
    );

    this.navigation_socket.onerror = function (event) {
      console.log("error navigation socket");
    };

    this.navigation_socket.onopen = function (event) {
      console.log("navigation socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.navigation_status.status = test["Status"];
        apiserv.navigation_status.avoided = test["Avoided"];
      };

      this.onclose = function () {
        console.log("navigation socket closed");
      };
    };

    ////////////////////// docking socket

    this.docking_status = new Docking();

    this.docking_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/docking/socket",
      "json"
    );

    this.docking_socket.onerror = function (event) {
      console.log("error docking socket");
    };

    this.docking_socket.onopen = function (event) {
      console.log("docking socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.docking_status.status = test["Status"];
        apiserv.docking_status.detected = test["Detected"];
      };

      this.onclose = function () {
        console.log("docking socket closed");
      };
    };

    ////////////////////// anticollision socket

    this.anticollision_status = new Anticollision();

    this.anticollision_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/anticollision/socket",
      "json"
    );

    this.anticollision_socket.onerror = function (event) {
      console.log("error anticollision socket");
    };

    this.anticollision_socket.onopen = function (event) {
      console.log("anticollision socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.anticollision_status.timestamp = test["Timestamp"];
        apiserv.anticollision_status.enabled = test["Enabled"];
        apiserv.anticollision_status.locked = test["Locked"];
        apiserv.anticollision_status.forward = test["Forward"];
        apiserv.anticollision_status.right = test["Right"];
        apiserv.anticollision_status.left = test["Left"];
      };

      this.onclose = function () {
        console.log("anticollision socket closed");
      };
    };
    ////////////////////// statistics socket

    this.statistics_status = new Statistics();

    this.statistics_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/statistics/socket",
      "json"
    );

    this.statistics_socket.onerror = function (event) {
      console.log("error statistic socket");
    };

    this.statistics_socket.onopen = function (event) {
      console.log("statistic socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.statistics_status.totalTime = test["TotalTime"];
        apiserv.statistics_status.totalDistance = test["TotalDistance"];
        apiserv.statistics_status.timestamp = test["Timestamp"];
      };

      this.onclose = function () {
        console.log("statistic socket closed");
      };
    };

    ////////////////////walker socket

    this.walker_states = new Walkassit();
    this.walker_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/walker/socket",
      "json"
    );
    this.walker_socket.onerror = function (event) {
      console.log("error walker socket");
    };

    this.walker_socket.onopen = function (event) {
      console.log("walker socket opened");
      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.walker_states.detected = test["Detected"];
        apiserv.walker_states.origin = test["Origin"];
        apiserv.walker_states.timestamp = test["Timestamp"];
        apiserv.walker_states.person = test["Person"];
        apiserv.walker_states.status = test["Status"];
        apiserv.walker_states.barsAngle = test["BarsAngle"];
        apiserv.walker_states.targetAngularSpeed = test["TargetAngularSpeed"];
        apiserv.walker_states.activationDistance = test["ActivationDistance"];
      };

      this.onclose = function () {
        console.log("walker socket closed");
      };
    };

    ///////////////////////// battery socket
    this.battery_status = new Battery();

    this.battery_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/battery/socket",
      "json"
    );

    this.battery_socket.onerror = function (event) {
      console.log("error battery socket");
    };

    this.battery_socket.onopen = function (event) {
      console.log("battery socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.battery_status.autonomy = test["Autonomy"];
        apiserv.battery_status.current = test["Current"];
        apiserv.battery_status.remaining = test["Remaining"];
        apiserv.battery_status.status = test["Status"];
        apiserv.battery_status.timestamp = test["Timestamp"];
        apiserv.battery_status.voltage = test["Voltage"];
      };

      this.onclose = function () {
        console.log("battery socket closed");
      };
    };
    //////////////////////////////// io socket
    this.b_pressed = false;
    this.btnPush = false;
    this.io_status = new Iostate();
    this.io_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/io/socket",
      "json"
    );
    this.io_socket.onerror = function (event) {
      console.log("error iosocket");
    };

    this.io_socket.onopen = function (event) {
      console.log("io socket opened");

      this.onclose = function () {
        console.log("io socket closed");
      };
    };

    this.io_socket.onmessage = function (event) {
      var test = JSON.parse(event.data);
      apiserv.io_status.timestamp = test["Timestamp"];
      apiserv.io_status.dIn = test["DIn"];
      apiserv.io_status.aIn = test["AIn"];

      if (apiserv.io_status.dIn[2] || apiserv.io_status.dIn[8]) {
        //smart button and button A of the game pad
        if (!apiserv.b_pressed) {
          apiserv.b_pressed = true;
          apiserv.btnPush = true;
        }
      } else {
        if (apiserv.b_pressed) {
          apiserv.b_pressed = false;
        }
      }
    };
  }

  mailAddInformationWalk() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 1" +
      "<br> Walker Status : " +
      this.walker_states.status +
      "<br> Walker Detect : " +
      this.walker_states.detected +
      "<br> Meter covered : " +
      this.metercovered +
      "<br>"
    );
  }
  mailAddInformationBasic() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 1" +
      "<br>"
    );
  }

  checkrobot() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
      .subscribe(
        (data) => {
          this.robotok = true;
        },
        (err) => {
          console.log(err);
          this.robotok = false;
        }
      );
  }
  jetsonOK() {
    this.checkInternet();
    this.alert.checkwifi(this.wifiok);
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
        observe: "response",
      })
      .subscribe(
        (resp) => {
          if (this.statusRobot === 2) {
            this.alert.appError(this.mailAddInformationBasic());
            document.location.reload(); //if error then refresh the page and relaunch websocket
          }
          if (resp.status === 200) {
            this.cpt_jetsonOK += 1;
            if (this.walker_states.status === 2) {
              // knee laser in error
              this.statusRobot = 1;
              this.connectionLost();
              this.walkercmdstopdetect(); //if we don't do that the knee laser will go wrong
            } else {
              this.statusRobot = 0; // everything is ok
              this.connect();
            }
          } else {
            this.cpt_jetsonOK += 1;
            if (this.cpt_jetsonOK > 5) {
              this.statusRobot = 2; //no connection
              this.connectionLost();
            }
          }
        },
        (err) => {
          console.log(err); //no connection
          this.cpt_jetsonOK += 1;
          if (this.cpt_jetsonOK > 5) {
            this.statusRobot = 2; //no connection
            this.connectionLost();
          }
        }
      );
  }

  async isConnectedInternet(): Promise<boolean> {
    try {
      const response = await fetch('http://localhost/ionicDB/internetping.php');
      const text = await response.text(); // Récupère le contenu texte de la réponse
      //console.log(text);
      
      // Analyse du texte pour déterminer la connectivité Internet
      return text.trim() === 'true'; // Renvoie true si le texte est 'true', sinon false
      
    } catch (error) {
      console.error('Error checking internet connectivity:', error);
      return false;
    }
  }
  checkInternet() {
    this.isConnectedInternet().then(connecte => {
      if (connecte) {
        //console.log("L'ordinateur est connecté à Internet");
        this.wifiok = true;
      } else {
        //console.log("L'ordinateur n'est pas connecté à Internet");
        this.wifiok = false;
      }
    });

  }

  walkercmdstart(speed : number) {
    // start legs enslavement
    //console.log("hello");
    var test = {
      Guided: false,
      Assist: true,
      Enable: true,
      MaxSpeed : speed,
      OriginX:this.selectWalkZone
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }

  walkercmdstartdetect() {
    // start leg detection
    //console.log("prout");
    var test = {
      Guided: false,
      Assist: false,
      Enable: true,
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }

  walkercmdstopdetect() {
    // stop leg enslavement + detection
    var test = {
      Guided: false,
      Assist: false,
      Enable: false,
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }

  connectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
        observe: "response",
      })
   
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("connectOK");
        },
        (err) => {
          console.log(err);
          console.log("connectPBM");
        }
      );
  }

  disconnectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
        observe: "response",
      })
     
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("disconnectOK");
        },
        (err) => {
          console.log(err);
          console.log("disconnectPBM");
        }
      );
  }

  abortNavHttp() {
    // stop the navigation
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortNavOK");
        },
        (err) => {
          console.log(err);
          console.log("abortNavPBM");
        }
      );
  }

  abortDockingHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortdockingOK");
        },
        (err) => {
          console.log(err);
          console.log("abortdockingPBM");
        }
      );
  }

  reachHttp(poiname: string) {
    this.httpClient
      .put(
        this.httpskomnav +
          this.param.localhost +
          "/api/navigation/destination/name/" +
          poiname,
        { observe: "response" }
      )
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("reachOK");
        },
        (err) => {
          console.log(err);
          console.log("reachPBM");
        }
      );
  }

  ngOnInit() {}


  connect() {
    this.is_connected = true;
  }

  connectionLost() {
    this.is_connected = false;
  }

  okToast(m: string, n: number) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: n,
      position: "middle",
      cssClass: "toastam",
    });
    toast.present();
  }

  eyesHttp(id: number) {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
  
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }

  
  deleteEyesHttp(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })
   
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }
}
