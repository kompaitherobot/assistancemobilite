import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { FreeAssistancePage } from "../free_assistance/free_assistance";
import { LoadingController } from "ionic-angular";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";
@Component({
  selector: "page-first",
  templateUrl: "first.html",
})
export class FirstPage implements OnInit {
  lowbattery: any;
  hour: any;
  freePage = FreeAssistancePage;
  appopen: any;
  cpt_battery: number;
  microon: boolean;
  batteryState: number;
  cpt_open: number;
  update: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  popup_battery: boolean;
  loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    public speech: SpeechService,
    public popup: PopupService,
    public param: ParamService,
    public alert: AlertService,
    public api: ApiService,
    public navCtrl: NavController
  ) {
    this.cpt_open = 0;
    this.cpt_battery = 0;
    this.loading = this.loadingCtrl.create({});
    this.loading.present(); //loading animation display
  }

  ngOnInit() {
    this.appopen = setInterval(() => this.appOpen(), 1000);
    this.speech.getVoice();
  }

  appOpen() {
    if (this.param.localhost != undefined) {
      this.api.checkrobot();
    }
    this.api.checkInternet();
    this.alert.checkwifi(this.api.wifiok);
    this.cpt_open += 1;
    if (this.cpt_open === 15) {
      this.popup.startFailedAlert();
      this.speech.speak(this.param.datatext.cantstart);
    }
    if (!this.api.appOpened) {
      this.param.getDataRobot();
      this.param.getMail();
      this.param.getPhone();
      this.param.getDurationNS();
      this.param.getBattery();
      if (
        this.param.robot &&
        this.param.duration &&
        this.param.maillist &&
        this.param.phonenumberlist
      ) {
        this.param.fillData();
        if (this.param.robot.httpskomnav == 0) {
          this.api.httpskomnav = "http://";
          this.api.wsskomnav = "ws://";
        } else {
          this.api.httpskomnav = "https://";
          this.api.wsskomnav = "wss://";
        }
        if (this.param.robot.httpsros == 0) {
          this.api.wssros = "ws://";
        } else {
          this.api.wssros = "wss://";
        }
      }
      if (this.api.robotok && this.param.langage && this.param.maillist) {
        this.api.instanciate();
        this.api.appOpened = true;
        this.api.eyesHttp(4);
      }
    } else if (this.api.appOpened && this.cpt_open >= 6) {
      this.alert.appOpen(this.api.mailAddInformationBasic());
      this.navCtrl.setRoot(this.freePage);
      
      this.loading.dismiss(); // destroy loading animation
      clearInterval(this.appopen);
      if (this.popup.alert_blocked) {
        this.popup.alert_blocked.dismiss();
      }
      console.log(this.cpt_open);
    }
  }
}
