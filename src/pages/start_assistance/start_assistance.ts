import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { FreeAssistancePage } from '../free_assistance/free_assistance';

@Component({
  selector: "page-startassistance",
  templateUrl: "start_assistance.html",
})
export class StartAssistancePage implements OnInit {

  cptduration: any;
  intervalcountdown: any;
  timecountdown: number; /// in sec
  is_sendmail: boolean;
  firstcount: boolean;

  loc: any;
  updateinterv: any;
  speechinterv: any;


  walkPage = FreeAssistancePage;


  constructor(
    public speech: SpeechService,
    public popup: PopupService,
    public alert: AlertService,
    public param: ParamService,
    public navCtrl: NavController,
    public api: ApiService,


  ) {
    this.updateinterv = setInterval(() => this.getUpdate(), 500);
    this.speechinterv = setInterval(() => this.sayorder(), 5000);
    this.api.inUse = true;
    this.is_sendmail = false;
    this.timecountdown = 3;
    this.speech.getVoice();
    // declecnche la popup de permission camera si besoin

    this.firstcount = false;
  }

  ngOnInit() {

  }


  ionViewWillLeave() {
    clearInterval(this.updateinterv);
    clearInterval(this.speechinterv);
  }

  ionViewDidEnter() {

  }

  sayorder() {
    if(this.api.position_state <2){
      if ( this.param.allowspeech == 0) {
      }else {
      this.speech.speak(this.param.datatext.closer);
    }
    }
  }

  getUpdate() {

    //console.log("lalal");
    if (!this.api.walker_states.detected) {
      this.api.position_state = 0;
      if (this.firstcount) {
        clearInterval(this.intervalcountdown);
        this.firstcount = false;
      }
      if (this.api.walker_states.status != 1) {
        this.api.walkercmdstartdetect();
      }
    }
    else {
      if (this.api.walker_states.person["X"] >this.api.selectWalkZone) {
        this.api.position_state = 1;
        if (this.firstcount) {
          clearInterval(this.intervalcountdown);
          this.firstcount = false;
        }
      }
      else {
        this.api.position_state = 2;
        if (!this.firstcount) {
          this.timecountdown = 3;
          this.firstcount = true;
          this.intervalcountdown = setInterval(
            () => this.updateCountdown(),
            1300
          );

        }
      }

    }

  }

  getDetect() {
    // if smart bouton is pressed or the bouton A of the gamepad is pressed



  }


  updateCountdown() {

    if (this.api.position_state === 2) {
      if (this.timecountdown > 0) {
        this.speech.speak(this.timecountdown.toString());
        this.api.okToast(this.timecountdown.toString(), 1300);
      } else if (this.timecountdown == 0) {
        this.api.okToast(this.param.datatext.btn_walk, 2000);
        this.speech.speak(this.param.datatext.walk);
      } else {
        this.Walk_page();
        clearInterval(this.intervalcountdown);
      }
    }
    if (this.timecountdown < -1) {
      clearInterval(this.intervalcountdown);
    }
    this.timecountdown--;
  }

  onclickstop(ev) {
    ev.preventDefault();
    this.api.inUse = false;
    clearInterval(this.intervalcountdown);
    this.navCtrl.popToRoot();
    this.api.walkercmdstartdetect();
  }


  Walk_page() {
    this.navCtrl.popToRoot();
    this.api.walkercmdstart(this.api.selectSpeed);
  }


}
