import { Component, ViewChild, ElementRef, Renderer2, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { StartAssistancePage } from '../start_assistance/start_assistance';
import { Select } from "ionic-angular";

@Component({
  selector: "page-freeassistance",
  templateUrl: "free_assistance.html",
})
export class FreeAssistancePage implements OnInit {
  updateMeter: any;
  cpt_locked: number; // cpt to know if kompai is blocked
  is_blocked: boolean;
  cptduration: any;
  intervalcountdown: any;
  timecountdown: number; /// in sec
  is_sendmail: boolean;
  firstcount: boolean;
  @ViewChild("video") videoElement: ElementRef;
  @ViewChild("canvas") canvas: ElementRef;
  videoWidth = 0;
  videoHeight = 0;
  selectOptions: any;
  selectOptionsWalkZone:any;
  startPage = StartAssistancePage;
  pageenter: boolean
  cptdetected:number=0;

  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;

  constructor(
    public speech: SpeechService,
    public popup: PopupService,
    public alert: AlertService,
    public param: ParamService,
    public navCtrl: NavController,
    public api: ApiService,
    private renderer: Renderer2

  ) {
    this.api.metercovered = 0;
    this.updateMeter = setInterval(() => this.getUpdate(), 500); // update meter and walker detection every 1/2 secondes
    this.cptduration = setInterval(() =>
    this.mail(), 120000); // toutes les 2 min actualise time
    this.api.inUse = true;
    this.is_sendmail = false;
    this.cpt_locked = 0;
    this.is_blocked = false;
    this.timecountdown = 3;
    
    // declecnche la popup de permission camera si besoin
    this.setupconstraint();
    this.firstcount = false;
    this.selectOptions = {
      title: this.param.datatext.choose_speed,

    };
    this.selectOptionsWalkZone = {
      title: this.param.datatext.choose_walkzone,
    };
  }

  mail(){
    this.param.cptDuration(); //update duration
    if(this.param.durationNS.length > 1){
      if(this.api.wifiok){
        this.alert.duration(this.param.durationNS[0],this.api.mailAddInformationBasic());
      }
    }
  }

  ngOnInit() {
    setTimeout(() => { 
      this.speech.speak(this.param.datatext.chosenfreemode);
    }, 1000);
    setTimeout(() => { 
      if(!this.api.inUse){
        this.speech.speak(this.param.datatext.letsstart);
      }
    }, 12000);
    this.api.walkercmdstartdetect();
  }

  triggerAssist(ev) {
    // Call preventDefault() to prevent any further handling
    ev.preventDefault();
    this.onclickassist();
  }

  ionViewWillEnter() {

   
    this.pageenter = false;
    setTimeout(() => {
      this.pageenter = true;
      
    }, 500);
  }
  
  ionViewWillLeave() {
    //clearInterval(this.updateMeter);
    //clearInterval(this.cptduration);
  }

  getUpdate() {
    //console.log(this.api.selectSpeed);
    if (this.api.close_app) {
      clearInterval(this.updateMeter);
      clearInterval(this.cptduration);
    }
    this.updateTrajectory();
    this.watchIfLocked();
    this.getDetect();
    this.getMeter();
    
  }


  watchIfLocked() {
    if (this.api.towardDocking) {
      if (
        this.api.docking_status.status === 2 ||
        this.api.docking_status.status === 3 ||
        (this.api.anticollision_status.forward < 2 &&
          this.api.anticollision_status.right < 2 &&
          this.api.anticollision_status.left < 2)
      ) {
        this.cpt_locked = 0;
        if (this.is_blocked) {
          this.is_blocked = false;
          this.popup.alert_blocked.dismiss();
        }
      } else if (
        this.api.anticollision_status.forward === 2 ||
        this.api.anticollision_status.right === 2 ||
        this.api.anticollision_status.left === 2
      ) {
        this.cpt_locked += 1;
      }
      if (this.cpt_locked > 40 && !this.is_blocked) {
        this.popup.blockedAlert();
        this.is_blocked = true;
        this.alert.blockingdocking(this.api.mailAddInformationBasic());
      } else if (this.cpt_locked === 100 && this.is_blocked) {
        this.captureBlocked();
      }
    }
  }

  sendHelp(ev) {
    ev.preventDefault();
    this.popup.askHelp();
    this.api.walkercmdstartdetect();
    // change the color and logo of the box
    this.api.inUse = false;

    if (!this.is_sendmail) {
      this.is_sendmail = true;
      // for (let num of this.param.phonenumberlist) {
      //   this.alert.sendsmsperso(this.param.datatext.smsSOS, num);
      // }

      // capture l'image et l'envoie via mail
      this.captureSOS();
      setTimeout(() => {
        this.is_sendmail = false;
      }, 30000);
    }
  }

  getDetect() {
    if (this.api.walker_states.status === 3) {
      // if (this.api.walker_states.person["X"] > this.api.walker_states.activationDistance + 0.12) {
      //   this.onclickassist();
      // }
      if (this.api.walker_states.person["X"] > this.api.walker_states.activationDistance || !this.api.walker_states.detected) {
        this.api.position_state = 1;//too far
      }
      else {
        this.api.position_state = 2;//in walkzone
      }
      if(!this.api.walker_states.detected){
        this.cptdetected=this.cptdetected+1;
      }
      else{
        this.cptdetected=0;
      }
    }
    // if smart bouton is pressed or the bouton A of the gamepad is pressed
    if (
      this.cptdetected>6 ||
      this.api.btnPush ||
      !this.api.is_connected
    ) {
      if (this.api.walker_states.status === 3) {
        this.cptdetected=0;
        // if (this.firstcount) {
        //   clearInterval(this.intervalcountdown);
        //   this.firstcount = false;
        // }
        //start the detection and stop the enslavement of the legs
        this.api.walkercmdstartdetect();
        // change the color and logo of the box
        this.api.inUse = false;
      }
      if (this.api.towardDocking && this.api.btnPush) {
        this.api.abortNavHttp();
        this.api.towardDocking = false;
        //this.api.walkercmdstartdetect();
      }
      this.api.btnPush = false;
    }
  }


  updateTrajectory() {
    // listen the round status to allow the robot to go to the next
    //// go docking or go poi in progress
    if (this.api.towardDocking) {
      this.api.appStart = false;
      if (this.api.differential_status.status === 2) {
        // if current is hight
        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.popup.errorBlocked();
        this.alert.errorblocked(this.api.mailAddInformationBasic());
        this.captureBlocked();
      } else if (this.api.navigation_status.status === 5) {
        // nav error

        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.popup.errorNavAlert();
        this.alert.naverror(this.api.mailAddInformationBasic());
      } else if (this.api.docking_status.status === 3) {
        this.api.walkercmdstopdetect();
        this.api.towardDocking = false;
        this.alert.charging(this.api.mailAddInformationBasic());
        this.api.close_app = true; //close application
        setTimeout(() => {
          window.close();
        }, 1000);
      } else if (
        this.api.towardDocking &&
        this.api.navigation_status.status === 0 &&
        this.api.docking_status.detected
      ) {
        //connect to the docking when he detect it
        this.api.connectHttp();
      }
    }
  }

  onRefreshMeter(ev) {
    ev.preventDefault();
    this.api.traveldebut = this.api.statistics_status.totalDistance;
    this.api.metercovered = 0;
  }

  getMeter() {
    if (this.api.traveldebut > 0) {
      this.api.metercovered =
        this.api.statistics_status.totalDistance - this.api.traveldebut;
    } else {
      this.api.traveldebut = this.api.statistics_status.totalDistance;
    }
  }

  onclickassist() {
    this.api.eyesHttp(4);
    if (this.api.towardDocking) {
      this.api.abortNavHttp();
      this.api.towardDocking = false;
      
      //this.api.walkercmdstartdetect();
    } else {
      this.alert.appInUse(this.api.mailAddInformationWalk());

      if (this.api.walker_states.status === 3) {
        this.pageenter = false;
        this.speech.speak(this.param.datatext.wantstop);
        this.api.walkercmdstartdetect();
        // change the color and logo of the box
        this.api.inUse = false;

        setTimeout(() => {
          this.pageenter = true;
        }, 600);
      } else {
        this.speech.synth.cancel();
        this.api.inUse = true;
        this.startAssistance_page();

      }
    }
  }

  startAssistance_page() {

    this.navCtrl.push(this.startPage);
    this.api.walkercmdstartdetect();
  }

  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,

    facingMode: "environment",
    width: { ideal: 1920 },
    height: { ideal: 1080 },
  };

  // config la variable constraints
  setupconstraint() {
    // demande de permission camera
    navigator.mediaDevices.getUserMedia(this.constraints);

    // verification des permissions video et de la disponibilité de devices
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // enumeration des appareils connectes pour filtrer
      navigator.mediaDevices.enumerateDevices().then((result) => {
        var constraints;
        console.log(result);

        result.forEach(function (device) {
          // filtrer pour garder les flux video
          if (device.kind.includes("videoinput")) {
            // filtrer le label de la camera
            if (device.label.includes("USB")) {
              constraints = {
                audio: false,
                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                // affecter le deviceid filtré
                video: { deviceId: device.deviceId },

                facingMode: "environment",
                width: { ideal: 1920 },
                height: { ideal: 1080 },
              };
            }
          }
        });
        // on lance la connexion de la vidéo
        this.startCamera(constraints);
      });
    } else {
      alert("Sorry, camera not available.");
    }
  }

  // function launch connection camera
  startCamera(cs) {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
      navigator.mediaDevices
        .getUserMedia(cs)
        .then(this.attachVideo.bind(this))
        .catch(this.handleError);
    } else {
      alert("Sorry, camera not available.");
    }
  }

  handleError(error) {
    console.log("Error: ", error);
  }

  // function qui attache le flux video à la balise video
  attachVideo(stream) {
    this.renderer.setProperty(
      this.videoElement.nativeElement,
      "srcObject",
      stream
    );
    this.renderer.listen(this.videoElement.nativeElement, "play", (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }

  // function take snapshot and send mail
  captureSOS() {
    this.alert.SOS_c();
    this.alert.SOS(this.api.mailAddInformationWalk());
  }

  captureBlocked() {
    if (this.param.robot.send_pic == 1) {
      this.setupconstraint();
      this.renderer.setProperty(
        this.canvas.nativeElement,
        "width",
        this.videoWidth
      );
      this.renderer.setProperty(
        this.canvas.nativeElement,
        "height",
        this.videoHeight
      );
      this.canvas.nativeElement
        .getContext("2d")
        .drawImage(this.videoElement.nativeElement, 0, 0);
      var url = this.canvas.nativeElement.toDataURL();
      console.log(url);
      this.alert.Blocked_c(url);
    } else {
      this.alert.Block_c();

    }
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
