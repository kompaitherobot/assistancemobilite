import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ComponentsModule } from '../components/components.module';
import { MyApp } from './app.component';
import { FirstPage } from '../pages/first/first';
import { HeadpageComponent } from '../components/headpage/headpage';
import { FreeAssistancePage } from '../pages/free_assistance/free_assistance';
import { ApiService } from '../services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { TutoPage } from '../pages/tuto/tuto';
import { PopupService } from '../services/popup.service';
import { AlertService } from '../services/alert.service';
import { ParamService } from '../services/param.service';
import { SpeechService } from '../services/speech.service';
import { ParamPage } from '../pages/param/param';
import { LanguePage } from '../pages/langue/langue';
import { SMSPage } from '../pages/sms/sms';
import { MailPage } from '../pages/mail/mail';
import { PasswordPage } from '../pages/password/password';
import { StartAssistancePage } from '../pages/start_assistance/start_assistance';

@NgModule({
  declarations: [
    MyApp,
    FirstPage,
    FreeAssistancePage,
    StartAssistancePage,
    TutoPage,
    LanguePage,
    PasswordPage,
    MailPage,
    SMSPage,
    ParamPage
    
  
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FirstPage,
    FreeAssistancePage,
    StartAssistancePage,
    TutoPage,
    ParamPage,
    LanguePage,
    PasswordPage,
    MailPage,
    SMSPage,
    HeadpageComponent
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    PopupService,
    AlertService,
    ParamService,
    SpeechService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
