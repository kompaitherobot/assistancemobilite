webpackJsonp([1],{

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// the parameter script








var ParamService = /** @class */ (function () {
    function ParamService(httpClient, sanitizer) {
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.tableduration = {};
        this.tablerobot = {};
        this.mail = {};
        this.phone = {};
        this.currentduration = {};
        this.durationNS = {}; //NS for not send
        this.tabledurationNS = {};
        this.maillist = [];
        this.phonenumberlist = [];
        this.cpt = 0;
        this.sendduration = false;
    }
    ParamService.prototype.getBattery = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(function (data) {
            _this.battery = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataRobot = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(function (data) {
            //console.log("robot:"+data);
            _this.robot = data[0];
            _this.langage = _this.robot.langage;
            if (_this.langage === "fr-FR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default.a;
            }
            else if (_this.langage === "en-GB") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default.a;
            }
            else if (_this.langage === "es-ES") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default.a;
            }
            else if (_this.langage === "el-GR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default.a;
            }
            else if (_this.langage === "de-DE") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default.a;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRobot = function () {
        this.tablerobot.action = "update";
        this.tablerobot.langage = this.robot.langage;
        this.tablerobot.serialnumber = this.serialnumber;
        this.tablerobot.name = this.name;
        this.tablerobot.send_pic = this.robot.send_pic;
        this.tablerobot.password = this.robot.password;
        this.tablerobot.allowspeech = this.robot.allowspeech;
        this.httpClient
            .post("http://localhost/ionicDB/updaterobotpassword.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getPhone = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(function (data) {
            if (_this.phonenumberlist.length === 0) {
                for (var value in data) {
                    _this.phonenumberlist.push(data[value].phonenumber);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addMail = function (m) {
        this.mail.action = "insert";
        this.mail.mail = m;
        this.mail.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteMail = function (m) {
        this.mail.action = "delete";
        this.mail.mail = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletemail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addPhone = function (m) {
        this.phone.action = "insert";
        this.phone.phonenumber = m;
        this.phone.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deletePhone = function (m) {
        this.phone.action = "delete";
        this.phone.phonenumber = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletephone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMail = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(function (data) {
            if (_this.maillist.length === 0) {
                for (var value in data) {
                    _this.maillist.push(data[value].mail);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDurationNS = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(function (data) {
            _this.duration = data;
            _this.durationNS = data; // NS =not send
            //console.log(data);
            //console.log(this.durationNS.length);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDurationNS = function (id) {
        this.tabledurationNS.action = "update";
        this.tabledurationNS.id_duration = id;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updateduration.php", JSON.stringify(this.tabledurationNS))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addDuration = function () {
        this.tableduration.action = "insert";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/addduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDuration = function () {
        this.tableduration.action = "update";
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updatedurationwalk.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.fillData = function () {
        this.name = this.robot.name;
        this.allowspeech = this.robot.allowspeech;
        this.maildatapassw = atob(this.robot.maildatapass);
        this.mailrobotpassw = atob(this.robot.mailrobotpass);
        this.localhost = this.robot.localhost;
        this.serialnumber = this.robot.serialnumber;
        this.robotmail = this.robot.mailrobot;
        this.datamail = this.robot.maildata;
        this.datamaillist = ["data@kompai.com"];
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_assistancemobility);
        if (this.duration.length > 0) {
            if (this.duration[this.duration.length - 1].date ===
                new Date().toLocaleDateString("fr-CA")) {
                this.currentduration.date = this.duration[this.duration.length - 1].date;
                this.currentduration.round = this.duration[this.duration.length - 1].round;
                this.currentduration.battery = this.duration[this.duration.length - 1].battery;
                this.currentduration.patrol = this.duration[this.duration.length - 1].patrol;
                this.currentduration.walk = this.duration[this.duration.length - 1].walk;
                this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
                this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
            }
            else {
                if (!this.sendduration) {
                    this.sendduration = true;
                    this.init_currentduration();
                    this.addDuration();
                }
            }
        }
        else {
            if (!this.sendduration) {
                this.sendduration = true;
                this.init_currentduration();
                this.addDuration();
            }
        }
    };
    ParamService.prototype.init_currentduration = function () {
        this.currentduration.round = 0;
        this.currentduration.battery = 0;
        this.currentduration.patrol = 0;
        this.currentduration.walk = 0;
        this.currentduration.toolbox = 0;
        this.currentduration.logistic = 0;
        this.currentduration.date = new Date().toLocaleDateString("fr-CA");
    };
    ParamService.prototype.cptDuration = function () {
        if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
            this.cpt = parseInt(this.currentduration.walk);
            this.currentduration.walk = this.cpt + 2;
            this.updateDuration();
        }
        else {
            this.init_currentduration();
            this.addDuration();
        }
    };
    ParamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ParamService);
    return ParamService;
}());

//# sourceMappingURL=param.service.js.map

/***/ }),

/***/ 131:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 131;

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_first_first__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, api, alert) {
        var _this = this;
        this.api = api;
        this.alert = alert;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_first_first__["a" /* FirstPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                // stop the enslavement of the legs if we do something else on the robot tablet
                _this.api.walkercmdstopdetect();
                window.close();
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__free_assistance_free_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FirstPage = /** @class */ (function () {
    function FirstPage(loadingCtrl, speech, popup, param, alert, api, navCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.speech = speech;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.navCtrl = navCtrl;
        this.freePage = __WEBPACK_IMPORTED_MODULE_5__free_assistance_free_assistance__["a" /* FreeAssistancePage */];
        this.cpt_open = 0;
        this.cpt_battery = 0;
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
    }
    FirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appopen = setInterval(function () { return _this.appOpen(); }, 1000);
        this.speech.getVoice();
    };
    FirstPage.prototype.appOpen = function () {
        if (this.param.localhost != undefined) {
            this.api.checkrobot();
        }
        this.api.checkInternet();
        this.alert.checkwifi(this.api.wifiok);
        this.cpt_open += 1;
        if (this.cpt_open === 15) {
            this.popup.startFailedAlert();
            this.speech.speak(this.param.datatext.cantstart);
        }
        if (!this.api.appOpened) {
            this.param.getDataRobot();
            this.param.getMail();
            this.param.getPhone();
            this.param.getDurationNS();
            this.param.getBattery();
            if (this.param.robot &&
                this.param.duration &&
                this.param.maillist &&
                this.param.phonenumberlist) {
                this.param.fillData();
                if (this.param.robot.httpskomnav == 0) {
                    this.api.httpskomnav = "http://";
                    this.api.wsskomnav = "ws://";
                }
                else {
                    this.api.httpskomnav = "https://";
                    this.api.wsskomnav = "wss://";
                }
                if (this.param.robot.httpsros == 0) {
                    this.api.wssros = "ws://";
                }
                else {
                    this.api.wssros = "wss://";
                }
            }
            if (this.api.robotok && this.param.langage && this.param.maillist) {
                this.api.instanciate();
                this.api.appOpened = true;
                this.api.eyesHttp(4);
            }
        }
        else if (this.api.appOpened && this.cpt_open >= 6) {
            this.alert.appOpen(this.api.mailAddInformationBasic());
            this.navCtrl.setRoot(this.freePage);
            this.loading.dismiss(); // destroy loading animation
            clearInterval(this.appopen);
            if (this.popup.alert_blocked) {
                this.popup.alert_blocked.dismiss();
            }
            console.log(this.cpt_open);
        }
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-first",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\first\first.html"*/'<ion-header no-border>\n\n \n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\first\first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartAssistancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_speech_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__free_assistance_free_assistance__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var StartAssistancePage = /** @class */ (function () {
    function StartAssistancePage(speech, popup, alert, param, navCtrl, api) {
        var _this = this;
        this.speech = speech;
        this.popup = popup;
        this.alert = alert;
        this.param = param;
        this.navCtrl = navCtrl;
        this.api = api;
        this.walkPage = __WEBPACK_IMPORTED_MODULE_7__free_assistance_free_assistance__["a" /* FreeAssistancePage */];
        this.updateinterv = setInterval(function () { return _this.getUpdate(); }, 500);
        this.speechinterv = setInterval(function () { return _this.sayorder(); }, 5000);
        this.api.inUse = true;
        this.is_sendmail = false;
        this.timecountdown = 3;
        this.speech.getVoice();
        // declecnche la popup de permission camera si besoin
        this.firstcount = false;
    }
    StartAssistancePage.prototype.ngOnInit = function () {
    };
    StartAssistancePage.prototype.ionViewWillLeave = function () {
        clearInterval(this.updateinterv);
        clearInterval(this.speechinterv);
    };
    StartAssistancePage.prototype.ionViewDidEnter = function () {
    };
    StartAssistancePage.prototype.sayorder = function () {
        if (this.api.position_state < 2) {
            if (this.param.allowspeech == 0) {
            }
            else {
                this.speech.speak(this.param.datatext.closer);
            }
        }
    };
    StartAssistancePage.prototype.getUpdate = function () {
        var _this = this;
        //console.log("lalal");
        if (!this.api.walker_states.detected) {
            this.api.position_state = 0;
            if (this.firstcount) {
                clearInterval(this.intervalcountdown);
                this.firstcount = false;
            }
            if (this.api.walker_states.status != 1) {
                this.api.walkercmdstartdetect();
            }
        }
        else {
            if (this.api.walker_states.person["X"] > this.api.selectWalkZone) {
                this.api.position_state = 1;
                if (this.firstcount) {
                    clearInterval(this.intervalcountdown);
                    this.firstcount = false;
                }
            }
            else {
                this.api.position_state = 2;
                if (!this.firstcount) {
                    this.timecountdown = 3;
                    this.firstcount = true;
                    this.intervalcountdown = setInterval(function () { return _this.updateCountdown(); }, 1300);
                }
            }
        }
    };
    StartAssistancePage.prototype.getDetect = function () {
        // if smart bouton is pressed or the bouton A of the gamepad is pressed
    };
    StartAssistancePage.prototype.updateCountdown = function () {
        if (this.api.position_state === 2) {
            if (this.timecountdown > 0) {
                this.speech.speak(this.timecountdown.toString());
                this.api.okToast(this.timecountdown.toString(), 1300);
            }
            else if (this.timecountdown == 0) {
                this.api.okToast(this.param.datatext.btn_walk, 2000);
                this.speech.speak(this.param.datatext.walk);
            }
            else {
                this.Walk_page();
                clearInterval(this.intervalcountdown);
            }
        }
        if (this.timecountdown < -1) {
            clearInterval(this.intervalcountdown);
        }
        this.timecountdown--;
    };
    StartAssistancePage.prototype.onclickstop = function (ev) {
        ev.preventDefault();
        this.api.inUse = false;
        clearInterval(this.intervalcountdown);
        this.navCtrl.popToRoot();
        this.api.walkercmdstartdetect();
    };
    StartAssistancePage.prototype.Walk_page = function () {
        this.navCtrl.popToRoot();
        this.api.walkercmdstart(this.api.selectSpeed);
    };
    StartAssistancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-startassistance",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\start_assistance\start_assistance.html"*/'<!-- mobility support html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="start_assistance"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding id="body">\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid no-padding class="heightstyle">\n\n      <ion-row class="heightstyle" id="firstrow">\n\n\n\n        <ion-col col-3>  \n\n          <button (mouseup)="onclickstop($event)"  class="btnstop">\n\n            <ion-icon color="light" name="hand" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_stop}}</p>\n\n            </ion-icon>\n\n          </button> \n\n\n\n        </ion-col>\n\n\n\n        <ion-col *ngIf="api.position_state===2" no-padding class="goodpose" >\n\n       \n\n        </ion-col>\n\n        <ion-col *ngIf="api.position_state!=2" no-padding class="badpose" >\n\n        \n\n        </ion-col>\n\n\n\n      </ion-row>\n\n\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\start_assistance\start_assistance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */]])
    ], StartAssistancePage);
    return StartAssistancePage;
}());

//# sourceMappingURL=start_assistance.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_free_assistance_free_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tuto_tuto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_param_param__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_speech_service__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var HeadpageComponent = /** @class */ (function () {
    function HeadpageComponent(speech, toastCtrl, alert, param, popup, navCtrl, api, sanitizer) {
        this.speech = speech;
        this.toastCtrl = toastCtrl;
        this.alert = alert;
        this.param = param;
        this.popup = popup;
        this.navCtrl = navCtrl;
        this.api = api;
        this.sanitizer = sanitizer;
        this.freePage = __WEBPACK_IMPORTED_MODULE_1__pages_free_assistance_free_assistance__["a" /* FreeAssistancePage */];
        this.tutoPage = __WEBPACK_IMPORTED_MODULE_4__pages_tuto_tuto__["a" /* TutoPage */];
        this.paramPage = __WEBPACK_IMPORTED_MODULE_8__pages_param_param__["a" /* ParamPage */];
        this.api.appStart = false;
        this.microon = false;
        this.cpt_battery = 0;
        this.cpt_open = 0;
    }
    HeadpageComponent.prototype.ionViewWillLeave = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.onTouchHelp = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking && this.api.walker_states.status != 3) {
            this.alert.displayTuto(this.api.mailAddInformationBasic());
            this.api.walkercmdstartdetect();
            this.navCtrl.push(this.tutoPage);
        }
    };
    HeadpageComponent.prototype.updateBattery = function () {
        if (this.api.statusRobot === 2) {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
        if (this.api.battery_status.status < 2) {
            //charging
            this.api.batteryState = 4;
        }
        else if (this.api.battery_status.status === 3 ||
            this.api.battery_status.remaining <= this.param.battery.critical) {
            //critical
            this.api.batteryState = 0;
        }
        else if (this.api.battery_status.remaining > this.param.battery.high) {
            //hight
            this.api.batteryState = 3;
        }
        else if (this.api.battery_status.remaining > this.param.battery.low) {
            //mean
            this.api.batteryState = 2;
        }
        else if (this.api.battery_status.remaining <= this.param.battery.low) {
            //low
            this.api.batteryState = 1;
        }
        else {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
    };
    HeadpageComponent.prototype.onTouchStatus = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking && this.api.walker_states.status != 3) {
            if (this.api.statusRobot > 0) {
                this.popup.statusRedPresent();
            }
            else {
                this.okToast(this.param.datatext.statusGreenPresent_message);
            }
        }
    };
    HeadpageComponent.prototype.accessparam = function () {
        this.navCtrl.push(this.paramPage);
    };
    HeadpageComponent.prototype.onTouchParam = function (ev) {
        ev.preventDefault();
        //this.speech.speak("Bonjour les amis. Comment ça va aujourd'hui ! ");
        if (!this.api.towardDocking && this.api.walker_states.status != 3) {
            if (this.pageName === "param" || this.pageName === "sms" || this.pageName === "mail" || this.pageName === "langue" || this.pageName === 'password') {
                console.log("already");
            }
            else {
                //this.navCtrl.push(this.paramPage);
                this.popup.askpswd();
            }
        }
    };
    HeadpageComponent.prototype.onTouchBattery = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking && this.api.walker_states.status != 3) {
            if (this.pageName === "free_assistance") {
                if (this.api.docking_status.status != 3) {
                    this.api.walkercmdstartdetect();
                    if (this.api.batteryState === 0) {
                        this.popup.lowBattery();
                    }
                    else {
                        this.popup.goDockingConfirm();
                    }
                }
                if (this.api.docking_status.status === 3) {
                    this.api.walkercmdstartdetect();
                    this.popup.leaveDockingConfirm();
                }
            }
            else {
                this.clicOnMenu();
            }
        }
        else {
            this.okToast(this.param.datatext.battery + " : " + this.api.battery_status.remaining + "%");
        }
    };
    HeadpageComponent.prototype.onTouchWifi = function (ev) {
        ev.preventDefault();
        if (!this.api.wifiok) {
            this.okToast(this.param.datatext.nointernet);
        }
        else {
            this.okToast(this.param.datatext.AlertConnectedToI);
        }
    };
    HeadpageComponent.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    HeadpageComponent.prototype.onTouchVolume = function (ev) {
        ev.preventDefault();
        this.speech.synth.cancel();
        if (this.api.volumeState === 0) {
            this.api.volumeState = 1;
            this.param.allowspeech = 1;
        }
        else {
            this.api.volumeState = 0;
            this.param.allowspeech = 0;
        }
    };
    HeadpageComponent.prototype.monitoringBattery = function () {
        if (this.api.batteryState === 0) {
            this.popup.lowBattery();
            this.alert.lowBattery(this.api.mailAddInformationBasic());
            this.alert.Battery_c();
            if (!this.api.towardDocking) {
                this.api.walkercmdstopdetect();
            }
            clearInterval(this.lowbattery);
        }
    };
    HeadpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.updateB = setInterval(function () { return _this.updateBand(); }, 500);
        this.popup.onSomethingHappened1(this.accessparam.bind(this));
        // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
        if (this.pageName === "free_assistance") {
            this.update = setInterval(function () { return _this.getUpdate(); }, 500); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.textBoutonHeader = this.param.datatext.quit;
        }
        else if (this.pageName === "start_assistance") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.gobehindrobot;
        }
        else if (this.pageName === "param") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.param;
        }
        else if (this.pageName === "sms") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivesms;
        }
        else if (this.pageName === "mail") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivemail;
        }
        else if (this.pageName === "langue") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.changelangage;
        }
        else if (this.pageName === "password") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.editpswd;
        }
        else {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.tutorial;
        }
        if (this.param.allowspeech == 1) {
            this.api.volumeState = 1;
        }
        else {
            this.api.volumeState = 0;
        }
    };
    HeadpageComponent.prototype.updateBand = function () {
        if (this.api.towardDocking) {
            this.textHeadBand = this.param.datatext.godocking;
        }
        else if (this.pageName === "free_assistance") {
            if (this.api.walker_states.status === 3) {
                this.textHeadBand = this.param.datatext.walkInProgress;
            }
            else {
                this.textHeadBand = this.param.datatext.notpush;
            }
        }
        else if (this.pageName === "start_assistance") {
            if (this.api.position_state !== 2) {
                this.textHeadBand = this.param.datatext.gobehindrobot;
            }
            else if (this.api.position_state === 2) {
                this.textHeadBand = this.param.datatext.wait;
            }
        }
    };
    HeadpageComponent.prototype.getUpdate = function () {
        // if(!this.api.towardDocking)
        // {
        //   if(this.api.walker_states.status===1)
        //   {
        //     this.api.appStart=true;
        //   }
        // if(this.pageName==="free_assistance" && this.api.walker_socket.readyState===1 && this.api.walker_states.status===0 && !this.api.appStart){
        //   //initialize the detection
        //   //this.api.walkercmdstartdetect();
        //   this.api.appStart=true;
        // }
        //}
        if (this.api.close_app) {
            clearInterval(this.update);
        }
        //this.updateBand();
        this.updateBattery();
        var now = new Date().toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric' });
        this.api.hour = now;
        if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
            this.cpt_battery += 1;
        }
        else {
            this.cpt_battery = 0;
        }
        if (this.cpt_battery > 10) {
            this.api.statusRobot = 2;
        }
    };
    HeadpageComponent.prototype.onquit = function (ev) {
        ev.preventDefault();
        this.clicOnMenu();
    };
    HeadpageComponent.prototype.clicOnMenu = function () {
        // the action depends on the page you are on
        if (this.pageName === 'tuto' || this.pageName === 'param') {
            //this.api.walkercmdstartdetect();
            this.navCtrl.popToRoot();
        }
        else if (this.pageName === 'sms' || this.pageName === 'mail' || this.pageName === 'langue' || this.pageName === 'password') {
            this.navCtrl.pop();
        }
        else if (this.pageName === 'start_assistance' || this.pageName === 'walk') {
            this.api.walkercmdstartdetect();
            this.navCtrl.popToRoot();
        }
        else {
            if (this.api.towardDocking) {
                this.api.abortNavHttp();
                this.api.towardDocking = false;
                this.popup.quitConfirm();
            }
            else {
                this.api.close_app = true;
                this.alert.appClosed(this.api.mailAddInformationBasic());
                //stop the enslavement of the legs and quit the app
                this.api.walkercmdstopdetect();
                setTimeout(function () {
                    window.close();
                }, 500);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HeadpageComponent.prototype, "pageName", void 0);
    HeadpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'headpage',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\components\headpage\headpage.html"*/'<!-- Html component of the HMI header with the Back / Exit button, the time, the sound buttons, microphone, parameters etc ...-->\n\n\n\n<ion-navbar hideBackButton>\n\n  <ion-buttons left class="btn_menu_size">\n\n    <button ion-button solid large class="btn_menu" (mouseup)="onquit($event)" >\n\n      <!-- the text of the button depends on the page you are on -->\n\n      <font class="font_menu_size">{{this.textBoutonHeader}}</font>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <ion-title text-center>\n\n    <font class="hour_size">{{api.hour}}</font>\n\n  </ion-title>\n\n\n\n  <ion-buttons right>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchHelp($event)" >\n\n      <img class="imgicon" src="./assets/imgs/help.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchParam($event)" >\n\n      <img class="imgicon" src="./assets/imgs/parameter.png" />\n\n    </button>\n\n    <!-- <button ion-button disabled class="btn_header" >\n\n      <img *ngIf="!microon" class="imgicon" src="./assets/imgs/microoff.png"/>\n\n      <img *ngIf="microon" class="imgicon" src="./assets/imgs/microon.png"/>\n\n    </button> -->\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWifi($event)" >\n\n      <img *ngIf="!api.wifiok" class="imginternet" src="./assets/imgs/wifioff.png" />\n\n      <img *ngIf="api.wifiok" class="imginternet" src="./assets/imgs/wifion.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchVolume($event)">\n\n      <img *ngIf="api.volumeState === 0" class="imgicon" src="./assets/imgs/volumemute.png"/>\n\n      <img *ngIf="api.volumeState === 1" class="imgicon" src="./assets/imgs/volumelow.png"/>\n\n      <img *ngIf="api.volumeState === 2" class="imgicon" src="./assets/imgs/volumehight.png"/>\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchStatus($event)" >\n\n      <img *ngIf="api.statusRobot===0" class="imgwifi" src="./assets/imgs/statusgreen.png" />\n\n      <img *ngIf="api.statusRobot===1" class="imgwifi" src="./assets/imgs/statusorange.png" />\n\n      <img *ngIf="api.statusRobot===2" class="imgwifi" src="./assets/imgs/statusred.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchBattery($event)" >\n\n      <img *ngIf="api.batteryState === 0" class="imgbattery" src="./assets/imgs/batteryoff.png" />\n\n      <img *ngIf="api.batteryState === 1" class="imgbattery" src="./assets/imgs/batterylow.png" />\n\n      <img *ngIf="api.batteryState === 2" class="imgbattery" src="./assets/imgs/batterymean.png" />\n\n      <img *ngIf="api.batteryState === 3" class="imgbattery" src="./assets/imgs/batteryhight.png" />\n\n      <img *ngIf="api.batteryState === 4" class="imgbattery" src="./assets/imgs/batterycharge.png" />\n\n    </button>\n\n  </ion-buttons>\n\n\n\n</ion-navbar>\n\n\n\n<ion-toolbar no-padding>\n\n  <div class="scroll_parent">\n\n    <ion-title class="scroll">\n\n      <font class="scroll_text">{{this.textHeadBand}}</font>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\components\headpage\headpage.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__services_speech_service__["a" /* SpeechService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_7__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_5__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HeadpageComponent);
    return HeadpageComponent;
}());

//# sourceMappingURL=headpage.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutoPage = /** @class */ (function () {
    function TutoPage(sanitizer, param) {
        this.sanitizer = sanitizer;
        this.param = param;
        this.pageNum = 1;
        this.pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
        this.PDFJSViewer = __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__;
        this.currPage = 1; //Pages are 1-based not 0-based
        this.numPages = 0;
        this.thePDF = null;
        this.pageRendering = false;
        this.pageNumPending = null;
    }
    TutoPage.prototype.ngOnInit = function () {
    };
    TutoPage.prototype.ionViewDidLoad = function () {
        // deuxième méthode
        this.launchdoc();
    };
    TutoPage.prototype.launchdoc = function () {
        var _this = this;
        //This is where you start
        __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__["getDocument"](this.param.datatext.URL_assistancemobility).then(function (pdf) {
            //Set PDFJS global object (so we can easily access in our page functions
            _this.thePDF = pdf;
            //How many pages it has
            _this.numPages = pdf.numPages;
            //Start with first page
            pdf.getPage(1).then(function (page) { _this.handlePages(page); });
        });
    };
    TutoPage.prototype.handlePages = function (page) {
        var _this = this;
        //This gives us the page's dimensions at full scale
        var viewport = page.getViewport(2);
        //We'll create a canvas for each page to draw it on
        var canvas = document.createElement("canvas");
        canvas.style.display = "block";
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        //Draw it on the canvas
        page.render({ canvasContext: context, viewport: viewport });
        //Add it to the web page
        //document.body.appendChild( canvas );
        //console.log(document.getElementById("ionbody"));
        document.getElementById("ionbody").appendChild(canvas);
        var line = document.createElement("hr");
        document.getElementById("ionbody").appendChild(line);
        //console.log(this.currPage);
        //Move to next page
        this.currPageAdd();
        if (this.thePDF !== null && this.currPage <= this.numPages) {
            this.thePDF.getPage(this.currPage).then(function (page) { _this.handlePages(page); });
        }
    };
    TutoPage.prototype.currPageAdd = function () {
        this.currPage = this.currPage + 1;
    };
    TutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tuto',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\tuto\tuto.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="tuto"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n  <ion-scroll scrollY="true" style="height:100%;width:80%;left:50%;transform:translateX(-50%); " >\n\n    <div class="center" style="height:100%;">\n\n      <div id="ionbody" style="display: flex;flex-grow: 1;  flex-direction: column;background: #ddd;overflow-y: auto;" width="60%" height="100%">\n\n\n\n      </div>\n\n    </div>\n\n  </ion-scroll>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\tuto\tuto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */]])
    ], TutoPage);
    return TutoPage;
}());

//# sourceMappingURL=tuto.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sms_sms__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langue_langue__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__password_password__ = __webpack_require__(237);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ParamPage = /** @class */ (function () {
    function ParamPage(navCtrl, api, param) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.param = param;
        this.mailPage = __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__["a" /* MailPage */];
        this.languePage = __WEBPACK_IMPORTED_MODULE_5__langue_langue__["a" /* LanguePage */];
        this.smsPage = __WEBPACK_IMPORTED_MODULE_4__sms_sms__["a" /* SMSPage */];
        this.passwordPage = __WEBPACK_IMPORTED_MODULE_7__password_password__["a" /* PasswordPage */];
    }
    ParamPage.prototype.ngOnInit = function () {
    };
    ParamPage.prototype.goMail = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.mailPage);
    };
    ParamPage.prototype.goSms = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.smsPage);
    };
    ParamPage.prototype.goLangue = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.languePage);
    };
    ParamPage.prototype.goPassword = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.passwordPage);
    };
    ParamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-param',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\param\param.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="param"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-list>\n\n        <button ion-item class="btnscss" (mouseup)="goMail($event)" >\n\n          <ion-icon class="iconscss" name="at" item-start></ion-icon>{{param.datatext.mails}}<ion-icon class="iconscss"\n\n            name="at" item-end></ion-icon>\n\n        </button>\n\n        <!-- <button ion-item class="btnscss" (click)="goSms()"><ion-icon  class="iconscss" name="chatboxes" item-start></ion-icon>{{param.datatext.sms}}<ion-icon  class="iconscss" name="chatboxes" item-end></ion-icon></button> -->\n\n        <button ion-item class="btnscss" (mouseup)="goLangue($event)" >\n\n          <ion-icon class="iconscss" name="globe" item-start></ion-icon>{{param.datatext.langage}}<ion-icon\n\n            class="iconscss" name="globe" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goPassword($event)" >\n\n          <ion-icon class="iconscss" name="key" item-start></ion-icon>{{param.datatext.editpswd1}}<ion-icon\n\n            class="iconscss" name="key" item-end></ion-icon>\n\n        </button>\n\n      </ion-list>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\param\param.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_param_service__["a" /* ParamService */]])
    ], ParamPage);
    return ParamPage;
}());

//# sourceMappingURL=param.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MailPage = /** @class */ (function () {
    function MailPage(param, popup, toastCtrl) {
        this.param = param;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
        this.inputmail = "";
    }
    MailPage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened2(this.addMail.bind(this));
    };
    MailPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    MailPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    MailPage.prototype.removeMail = function (ev, m) {
        ev.preventDefault();
        var index = this.param.maillist.indexOf(m);
        if (index !== -1) {
            this.param.maillist.splice(index, 1);
            this.param.deleteMail(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    MailPage.prototype.is_a_mail = function (m) {
        var mailformat = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        return m.match(mailformat);
    };
    MailPage.prototype.addMail = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.is_a_mail(this.inputmail)) {
            var index = this.param.maillist.indexOf(this.inputmail);
            if (index !== -1) {
                this.errorToast(this.param.datatext.mailexist);
            }
            else {
                this.param.maillist.push(this.inputmail);
                this.param.addMail(this.inputmail);
                this.okToast(this.param.datatext.mailadd);
                setTimeout(function () {
                    _this.content1.scrollToBottom();
                }, 300);
            }
        }
        else {
            this.errorToast(this.param.datatext.mailincorrect);
        }
    };
    MailPage.prototype.update_send_pic = function () {
        if (this.param.robot.send_pic == 1) {
            this.param.robot.send_pic = 0;
        }
        else {
            this.param.robot.send_pic = 1;
        }
        this.param.updateRobot();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], MailPage.prototype, "content1", void 0);
    MailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mail',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\mail\mail.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="mail"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="big-ion-content" padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n      <ion-item >\n\n        <ion-label class="labelcss" stacked>{{param.datatext.receivepic}}</ion-label>\n\n        <ion-toggle\n\n              [checked]="this.param.robot.send_pic ==1 "\n\n              (ionChange)="update_send_pic()"\n\n              item-end\n\n            ></ion-toggle>\n\n            <ion-icon\n\n              class="iconscss"\n\n              name="camera"\n\n              item-start\n\n              color="primary"\n\n            ></ion-icon>\n\n      </ion-item>\n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newmail}}</ion-label>\n\n          <ion-input inputmode="email" [(ngModel)]="inputmail" placeholder="kompai@gmail.com"></ion-input>\n\n          <button *ngIf="is_a_mail(inputmail)" class="btn-add" (mouseup)="popup.displayrgpd($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_mail(inputmail))" class="mailnotvalid" (mouseup)="addMail($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n        <ion-content\n\n        #content1\n\n        style="height: 80%; width: 100%; background-color: transparent;"\n\n      >\n\n        \n\n          <ion-item *ngFor="let m of this.param.maillist">\n\n            <ion-icon class="iconscss" name="at" item-start color="primary"></ion-icon>{{m}}\n\n            <button class="btn-trash" item-end (mouseup)="removeMail($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        \n\n        </ion-content>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\mail\mail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], MailPage);
    return MailPage;
}());

//# sourceMappingURL=mail.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SMSPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SMSPage = /** @class */ (function () {
    function SMSPage(param, toastCtrl) {
        this.param = param;
        this.toastCtrl = toastCtrl;
    }
    SMSPage.prototype.ngOnInit = function () {
    };
    SMSPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    SMSPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    SMSPage.prototype.removeNum = function (ev, m) {
        ev.preventDefault();
        var index = this.param.phonenumberlist.indexOf(m);
        if (index !== -1) {
            this.param.phonenumberlist.splice(index, 1);
            this.param.deletePhone(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    SMSPage.prototype.is_a_num = function (m) {
        var numformat = "^[0-9]{10}$";
        return m.match(numformat);
    };
    SMSPage.prototype.changeNum = function (m) {
        return "+33" + m.substring(1);
    };
    SMSPage.prototype.displayNum = function (m) {
        return "0" + m.substring(3);
    };
    SMSPage.prototype.addNum = function (ev, m) {
        ev.preventDefault();
        if (this.is_a_num(m)) {
            var index = this.param.phonenumberlist.indexOf(this.changeNum(m));
            if (index !== -1) {
                this.errorToast(this.param.datatext.numexist);
            }
            else if (this.param.phonenumberlist.length > 5) {
                this.errorToast(this.param.datatext.deletenum);
            }
            else {
                this.param.phonenumberlist.push(this.changeNum(m));
                this.param.addPhone(this.changeNum(m));
                this.okToast(this.param.datatext.numadd);
            }
        }
        else {
            this.errorToast(this.param.datatext.numincorrect);
        }
    };
    SMSPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sms',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\sms\sms.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="sms"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n     \n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newnum}}</ion-label>\n\n          <ion-input #tel  inputmode="tel" placeholder="0630854706"></ion-input>\n\n          <button *ngIf="is_a_num(tel.value)" class="btn-add" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_num(tel.value))" class="mailnotvalid" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n      \n\n        <ion-list >\n\n          <ion-item *ngFor="let m of this.param.phonenumberlist">\n\n            <ion-icon class="iconscss" name="chatboxes" item-start color="primary"></ion-icon>{{displayNum(m)}}\n\n            <button class="btn-trash" item-end (mouseup)="removeNum($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        </ion-list>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\sms\sms.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], SMSPage);
    return SMSPage;
}());

//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguePage = /** @class */ (function () {
    function LanguePage(param, loadingCtrl) {
        this.param = param;
        this.loadingCtrl = loadingCtrl;
        this.monitor = this.param.langage;
    }
    LanguePage.prototype.ngOnInit = function () {
    };
    LanguePage.prototype.monitorHandler = function () {
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
        this.param.updateRobot();
        setTimeout(function () {
            document.location.reload();
        }, 1000);
    };
    LanguePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], LanguePage.prototype, "select1", void 0);
    LanguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-langue',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\langue\langue.html"*/'<!-- langue page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="langue"></headpage>\n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-grid class="main_ion_grid">\n\n    <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 40%; width: 60%; ">\n\n      <ion-row style="height: 25%; color: rgb(0, 0, 0);justify-content: center!important; align-items: center!important; font-size: 4em;">\n\n        {{param.datatext.langage}}\n\n      </ion-row>\n\n      <ion-row style="height: 75%;vertical-align: middle;\n\n      justify-content: center;\n\n      display: flex!important;\n\n      align-items: center!important">\n\n       \n\n        <ion-select #select1 [(ngModel)]="param.robot.langage" (ionChange)="monitorHandler()" style="width: 70% !important; font-size: x-large;background-color:rgba(26, 156, 195, 0.199);" interface="popover" (mouseup)="onSliderRelease($event,select1)">\n\n          <ion-option value="de-DE">Deutsch</ion-option>\n\n          <ion-option value="el-GR">Ελληνικά</ion-option>\n\n          <ion-option value="en-GB">English</ion-option>\n\n          <ion-option value="fr-FR">Français</ion-option>\n\n          <ion-option value="es-ES">Spanish</ion-option>\n\n        </ion-select>\n\n     \n\n      </ion-row>\n\n    </div>\n\n   </ion-grid>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\langue\langue.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]])
    ], LanguePage);
    return LanguePage;
}());

//# sourceMappingURL=langue.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, popup, param) {
        this.navCtrl = navCtrl;
        this.popup = popup;
        this.param = param;
        this.newpassword = "";
        this.warn = false;
    }
    PasswordPage.prototype.ngOnInit = function () {
    };
    PasswordPage.prototype.editpassword = function (ev) {
        ev.preventDefault();
        if (this.password === atob(this.param.robot.password)) {
            if (this.newpassword.length >= 2) {
                this.popup.showToast(this.param.datatext.pswdsaved, 3000, "middle");
                this.param.robot.password = btoa(this.newpassword);
                this.param.updateRobot();
                this.password = "";
                this.newpassword = "";
                this.navCtrl.pop();
            }
        }
        else {
            this.warn = true;
        }
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\password\password.html"*/'<!-- password page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="password"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-grid class="main_ion_grid">\n\n        <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 60%; width: 60%; ">\n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.currentpswd}}</ion-label>\n\n          <ion-input minlength=2 maxlength=15 [(ngModel)]="password" style="font-size: xx-large; color: rgb(26, 156, 195);"  required></ion-input>\n\n       </ion-item>\n\n          \n\n        </ion-row>\n\n        \n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.newpswd}}</ion-label>\n\n          <ion-input [(ngModel)]="newpassword" minlength=2 maxlength=15 style="font-size: xx-large; color: rgb(26, 156, 195);" required></ion-input>\n\n          </ion-item>\n\n          \n\n        </ion-row>\n\n       <ion-row style="height: 20%; justify-content: center!important; ">\n\n          <p *ngIf="warn" style="color: red; font-size: large;">** {{param.datatext.wrongpass}} **</p>\n\n        </ion-row>\n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <button [disabled]="2>this.newpassword.length" ion-button solid large class="btn_login" (mouseup)="editpassword($event)"> \n\n            <!-- the text of the button depends on the page you are on -->\n\n            <font class="font_menu_size" >{{param.datatext.save}}</font>\n\n          </button>\n\n     \n\n        </ion-row>\n\n        </div>\n\n       </ion-grid>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(259);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_first_first__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_free_assistance_free_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tuto_tuto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_speech_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_param_param__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_password_password__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_start_assistance_start_assistance__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_free_assistance_free_assistance__["a" /* FreeAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_start_assistance_start_assistance__["a" /* StartAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_free_assistance_free_assistance__["a" /* FreeAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_start_assistance_start_assistance__["a" /* StartAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__["a" /* HeadpageComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_10__services_api_service__["a" /* ApiService */],
                __WEBPACK_IMPORTED_MODULE_13__services_popup_service__["a" /* PopupService */],
                __WEBPACK_IMPORTED_MODULE_14__services_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_15__services_param_service__["a" /* ParamService */],
                __WEBPACK_IMPORTED_MODULE_16__services_speech_service__["a" /* SpeechService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Walkassit */
/* unused harmony export Person */
/* unused harmony export Origin */
/* unused harmony export Docking */
/* unused harmony export Battery */
/* unused harmony export Anticollision */
/* unused harmony export Statistics */
/* unused harmony export Iostate */
/* unused harmony export Navigation */
/* unused harmony export Localization */
/* unused harmony export Differential */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(12);
// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var Walkassit = /** @class */ (function () {
    function Walkassit() {
    }
    return Walkassit;
}());

var Person = /** @class */ (function () {
    function Person() {
    }
    return Person;
}());

var Origin = /** @class */ (function () {
    function Origin() {
    }
    return Origin;
}());

var Docking = /** @class */ (function () {
    function Docking() {
    }
    return Docking;
}());

/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/
var Battery = /** @class */ (function () {
    function Battery() {
    }
    return Battery;
}());

var Anticollision = /** @class */ (function () {
    function Anticollision() {
    }
    return Anticollision;
}());

var Statistics = /** @class */ (function () {
    function Statistics() {
    }
    return Statistics;
}());

var Iostate = /** @class */ (function () {
    function Iostate() {
    }
    return Iostate;
}());

var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    return Navigation;
}());

var Localization = /** @class */ (function () {
    function Localization() {
    }
    return Localization;
}());

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.
var Differential = /** @class */ (function () {
    function Differential() {
    }
    return Differential;
}());

var ApiService = /** @class */ (function () {
    function ApiService(toastCtrl, app, httpClient, alert, param) {
        this.toastCtrl = toastCtrl;
        this.app = app;
        this.httpClient = httpClient;
        this.alert = alert;
        this.param = param;
        this.position_state = 0; //0 ?  1 bad 2 good
        this.selectSpeed = 0.5;
        this.selectWalkZone = 0.55;
        this.is_connected = false; // to know if we are connected to the robot
        this.towardDocking = false; //to know if the robot is moving toward the docking
        this.traveldebut = 0;
        this.statusRobot = 10;
        this.metercovered = 0;
        this.cpt_jetsonOK = 0;
        this.appStart = false;
        this.close_app = false;
        this.appOpened = false;
        this.inUse = false; // to know if the enslavement of the legs is active or not
        this.robotok = false;
    }
    ApiService.prototype.instanciate = function () {
        var apiserv = this;
        ////////////////////// localization socket
        this.localization_status = new Localization();
        this.localization_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/localization/socket", "json");
        this.localization_socket.onerror = function (event) {
            console.log("error localization socket");
        };
        this.localization_socket.onopen = function (event) {
            console.log("localization socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.localization_status.positionx = test["Pose"]["X"];
                apiserv.localization_status.positiony = test["Pose"]["Y"];
                apiserv.localization_status.positiont = test["Pose"]["T"];
            };
            this.onclose = function () {
                console.log("localization socket closed");
            };
        };
        ////////////////////// differential socket
        this.differential_status = new Differential();
        this.differential_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/differential/socket", "json");
        this.differential_socket.onerror = function (event) {
            console.log("error differential socket");
        };
        this.differential_socket.onopen = function (event) {
            console.log("differential socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.differential_status.status = test["Status"];
            };
            this.onclose = function () {
                console.log("differential socket closed");
            };
        };
        ////////////////////// navigation socket
        this.navigation_status = new Navigation();
        this.navigation_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/navigation/socket", "json");
        this.navigation_socket.onerror = function (event) {
            console.log("error navigation socket");
        };
        this.navigation_socket.onopen = function (event) {
            console.log("navigation socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.navigation_status.status = test["Status"];
                apiserv.navigation_status.avoided = test["Avoided"];
            };
            this.onclose = function () {
                console.log("navigation socket closed");
            };
        };
        ////////////////////// docking socket
        this.docking_status = new Docking();
        this.docking_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/docking/socket", "json");
        this.docking_socket.onerror = function (event) {
            console.log("error docking socket");
        };
        this.docking_socket.onopen = function (event) {
            console.log("docking socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.docking_status.status = test["Status"];
                apiserv.docking_status.detected = test["Detected"];
            };
            this.onclose = function () {
                console.log("docking socket closed");
            };
        };
        ////////////////////// anticollision socket
        this.anticollision_status = new Anticollision();
        this.anticollision_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/anticollision/socket", "json");
        this.anticollision_socket.onerror = function (event) {
            console.log("error anticollision socket");
        };
        this.anticollision_socket.onopen = function (event) {
            console.log("anticollision socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.anticollision_status.timestamp = test["Timestamp"];
                apiserv.anticollision_status.enabled = test["Enabled"];
                apiserv.anticollision_status.locked = test["Locked"];
                apiserv.anticollision_status.forward = test["Forward"];
                apiserv.anticollision_status.right = test["Right"];
                apiserv.anticollision_status.left = test["Left"];
            };
            this.onclose = function () {
                console.log("anticollision socket closed");
            };
        };
        ////////////////////// statistics socket
        this.statistics_status = new Statistics();
        this.statistics_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/statistics/socket", "json");
        this.statistics_socket.onerror = function (event) {
            console.log("error statistic socket");
        };
        this.statistics_socket.onopen = function (event) {
            console.log("statistic socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.statistics_status.totalTime = test["TotalTime"];
                apiserv.statistics_status.totalDistance = test["TotalDistance"];
                apiserv.statistics_status.timestamp = test["Timestamp"];
            };
            this.onclose = function () {
                console.log("statistic socket closed");
            };
        };
        ////////////////////walker socket
        this.walker_states = new Walkassit();
        this.walker_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/walker/socket", "json");
        this.walker_socket.onerror = function (event) {
            console.log("error walker socket");
        };
        this.walker_socket.onopen = function (event) {
            console.log("walker socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.walker_states.detected = test["Detected"];
                apiserv.walker_states.origin = test["Origin"];
                apiserv.walker_states.timestamp = test["Timestamp"];
                apiserv.walker_states.person = test["Person"];
                apiserv.walker_states.status = test["Status"];
                apiserv.walker_states.barsAngle = test["BarsAngle"];
                apiserv.walker_states.targetAngularSpeed = test["TargetAngularSpeed"];
                apiserv.walker_states.activationDistance = test["ActivationDistance"];
            };
            this.onclose = function () {
                console.log("walker socket closed");
            };
        };
        ///////////////////////// battery socket
        this.battery_status = new Battery();
        this.battery_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/battery/socket", "json");
        this.battery_socket.onerror = function (event) {
            console.log("error battery socket");
        };
        this.battery_socket.onopen = function (event) {
            console.log("battery socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.battery_status.autonomy = test["Autonomy"];
                apiserv.battery_status.current = test["Current"];
                apiserv.battery_status.remaining = test["Remaining"];
                apiserv.battery_status.status = test["Status"];
                apiserv.battery_status.timestamp = test["Timestamp"];
                apiserv.battery_status.voltage = test["Voltage"];
            };
            this.onclose = function () {
                console.log("battery socket closed");
            };
        };
        //////////////////////////////// io socket
        this.b_pressed = false;
        this.btnPush = false;
        this.io_status = new Iostate();
        this.io_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/io/socket", "json");
        this.io_socket.onerror = function (event) {
            console.log("error iosocket");
        };
        this.io_socket.onopen = function (event) {
            console.log("io socket opened");
            this.onclose = function () {
                console.log("io socket closed");
            };
        };
        this.io_socket.onmessage = function (event) {
            var test = JSON.parse(event.data);
            apiserv.io_status.timestamp = test["Timestamp"];
            apiserv.io_status.dIn = test["DIn"];
            apiserv.io_status.aIn = test["AIn"];
            if (apiserv.io_status.dIn[2] || apiserv.io_status.dIn[8]) {
                //smart button and button A of the game pad
                if (!apiserv.b_pressed) {
                    apiserv.b_pressed = true;
                    apiserv.btnPush = true;
                }
            }
            else {
                if (apiserv.b_pressed) {
                    apiserv.b_pressed = false;
                }
            }
        };
    };
    ApiService.prototype.mailAddInformationWalk = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 1" +
            "<br> Walker Status : " +
            this.walker_states.status +
            "<br> Walker Detect : " +
            this.walker_states.detected +
            "<br> Meter covered : " +
            this.metercovered +
            "<br>");
    };
    ApiService.prototype.mailAddInformationBasic = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 1" +
            "<br>");
    };
    ApiService.prototype.checkrobot = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
            .subscribe(function (data) {
            _this.robotok = true;
        }, function (err) {
            console.log(err);
            _this.robotok = false;
        });
    };
    ApiService.prototype.jetsonOK = function () {
        var _this = this;
        this.checkInternet();
        this.alert.checkwifi(this.wifiok);
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
            observe: "response",
        })
            .subscribe(function (resp) {
            if (_this.statusRobot === 2) {
                _this.alert.appError(_this.mailAddInformationBasic());
                document.location.reload(); //if error then refresh the page and relaunch websocket
            }
            if (resp.status === 200) {
                _this.cpt_jetsonOK += 1;
                if (_this.walker_states.status === 2) {
                    // knee laser in error
                    _this.statusRobot = 1;
                    _this.connectionLost();
                    _this.walkercmdstopdetect(); //if we don't do that the knee laser will go wrong
                }
                else {
                    _this.statusRobot = 0; // everything is ok
                    _this.connect();
                }
            }
            else {
                _this.cpt_jetsonOK += 1;
                if (_this.cpt_jetsonOK > 5) {
                    _this.statusRobot = 2; //no connection
                    _this.connectionLost();
                }
            }
        }, function (err) {
            console.log(err); //no connection
            _this.cpt_jetsonOK += 1;
            if (_this.cpt_jetsonOK > 5) {
                _this.statusRobot = 2; //no connection
                _this.connectionLost();
            }
        });
    };
    ApiService.prototype.isConnectedInternet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, text, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch('http://localhost/ionicDB/internetping.php')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        text = _a.sent();
                        //console.log(text);
                        // Analyse du texte pour déterminer la connectivité Internet
                        return [2 /*return*/, text.trim() === 'true']; // Renvoie true si le texte est 'true', sinon false
                    case 3:
                        error_1 = _a.sent();
                        console.error('Error checking internet connectivity:', error_1);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.checkInternet = function () {
        var _this = this;
        this.isConnectedInternet().then(function (connecte) {
            if (connecte) {
                //console.log("L'ordinateur est connecté à Internet");
                _this.wifiok = true;
            }
            else {
                //console.log("L'ordinateur n'est pas connecté à Internet");
                _this.wifiok = false;
            }
        });
    };
    ApiService.prototype.walkercmdstart = function (speed) {
        // start legs enslavement
        //console.log("hello");
        var test = {
            Guided: false,
            Assist: true,
            Enable: true,
            MaxSpeed: speed,
            OriginX: this.selectWalkZone
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.walkercmdstartdetect = function () {
        // start leg detection
        //console.log("prout");
        var test = {
            Guided: false,
            Assist: false,
            Enable: true,
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.walkercmdstopdetect = function () {
        // stop leg enslavement + detection
        var test = {
            Guided: false,
            Assist: false,
            Enable: false,
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.connectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("connectOK");
        }, function (err) {
            console.log(err);
            console.log("connectPBM");
        });
    };
    ApiService.prototype.disconnectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("disconnectOK");
        }, function (err) {
            console.log(err);
            console.log("disconnectPBM");
        });
    };
    ApiService.prototype.abortNavHttp = function () {
        // stop the navigation
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortNavOK");
        }, function (err) {
            console.log(err);
            console.log("abortNavPBM");
        });
    };
    ApiService.prototype.abortDockingHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortdockingOK");
        }, function (err) {
            console.log(err);
            console.log("abortdockingPBM");
        });
    };
    ApiService.prototype.reachHttp = function (poiname) {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/name/" +
            poiname, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("reachOK");
        }, function (err) {
            console.log(err);
            console.log("reachPBM");
        });
    };
    ApiService.prototype.ngOnInit = function () { };
    ApiService.prototype.connect = function () {
        this.is_connected = true;
    };
    ApiService.prototype.connectionLost = function () {
        this.is_connected = false;
    };
    ApiService.prototype.okToast = function (m, n) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: n,
            position: "middle",
            cssClass: "toastam",
        });
        toast.present();
    };
    ApiService.prototype.eyesHttp = function (id) {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.deleteEyesHttp = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */]])
    ], ApiService);
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__headpage_headpage__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */])],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 309:
/***/ (function(module, exports) {

module.exports = {"quit":"QUITTER","battery":"BATTERIE","return":"RETOUR","tutorial":"TUTORIEL","param":"PARAMÈTRES","receivesms":"RECEVOIR DES ALERTES SMS","receivemail":"RECEVOIR DES ALERTES PAR MAIL","changelangage":"CHANGER LA LANGUE","godocking":"EN ROUTE VERS LA STATION DE CHARGE","round":"TOURNÉE DE DIVERTISSEMENT","patrol":"Patrouille","patrolInProgress":"PATROUILLE EN COURS","roundInProgress":"TOURNÉE DE DIVERTISSEMENT EN COURS","moving":"DÉPLACEMENT EN COURS VERS :","charging_remaining":"ROBOT EN CHARGE - BATTERIE: ","walkInProgress":"MARCHEZ A VOTRE RYTHME SANS POUSSER","notpush":"INSTALLEZ VOUS COMME SUR LA PHOTO ET APPUYEZ SUR «ALLONS-Y»","mails":"Mails","sms":"SMS","langage":"Langue","distance_covered":"Distance parcourue","goto":"Se rendre au :","inProgress":"EN COURS","post":"Poste","sentinel":"Sentinelle","morningRound":"Tournée du matin","eveningRound":"Tournée du soir","btn_go":"LANCER","btn_help":"Aide","btn_walk":"Allons-y !","btn_stop":"STOP","btn_charge":"CHARGER","btn_cancel":"Annuler","btn_ok":"OK","btn_yes":"OUI","btn_no":"NON","numexist":"Ce numéro existe déjà !","deletenum":"Veuillez supprimer un numéro d'abord","numadd":"Numéro ajouté","numincorrect":"Numéro invalide","newnum":"Nouveau numéro","mailexist":"Cette adresse existe déjà ! ","deletemail":"Veuillez supprimer une adresse d'abord","mailadd":"Adresse mail ajoutée !","mailincorrect":"Adresse mail invalide","newmail":"Nouvelle adresse","error":"Erreur","cantstart":"Impossible de démarrer l'application. Vérifiez que le robot est allumé correctement.","nointernet":"Pas d'internet","btn_save":"Sauver","choose_speed":"Choisir la vitesse","speed":"Vitesse","receivepic":"Recevoir des photos","editpswd":"CHANGER LE MOT DE PASSE","currentpswd":"Mot de passe actuel :","newpswd":"Nouveau mot de passe :","save":"Sauvegarder","success":"C'est Fait !","pswdsaved":"Mot de passe sauvegardé","langage1":"Changer la langue","editpswd1":"Changer le mot de passe","wrongpass":"Mauvais mot de passe","gobehindrobot":"AVANCEZ VOUS DANS LA ZONE DE MARCHE","choosedestination":"CHOISIR LA DESTINATION ET APPUYER SUR «ALLONS-Y»","chooseround":"CHOISIR LA RONDE ET APPUYER SUR «ALLONS-Y»","gobackward":"VEUILLEZ RECULER, VOUS ETES TROP PRES DU ROBOT","wait":"ATTENDRE LA FIN DU DÉCOMPTE AVANT DE MARCHER","stepback":"Vous êtes trop près du robot.","closer":"Veuillez vous rapprochez, le robot ne vous détecte pas.","choose_walkzone":"Modifier la zone de marche","walkzone":"Zone de marche","smsSOS":"Quelqu'un a appuyé sur le bouton SOS du robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Une personne a besoin d'aide","mailBattery_suj":"Batterie Faible","mailBattery_body":"<br> Le robot a besoin d'etre chargé. Veuillez le mettre sur sa station de charge.","mailBlocked_suj":"Robot bloqué","mailBlocked_body":"<br> Le robot est bloqué <br>","mailFall_body":"Le robot détecte une personne à terre <br>","mailFall_suj":"CHUTE détectée","mailPerson_suj":"Personne détectée","mailPerson_body":"Le robot a détecté une personne <br>","mailRemove":"Suppression réussie","presentAlert_title":"Robot en charge","presentAlert_message":"Attention je vais reculer !","AlertConnectedToI":"Le robot est connecté à internet","presentConfirm_title":"Ronde suspendue","presentConfirm_message":"Reprendre la ronde ?","FallConfirm_title":"Chute détectée !","FallConfirm_message":"Reprendre la patrouille ?","RemoteConfirm_title":"Ronde arrêtée à distance","RemoteConfirm_message":"Reprendre la ronde ?","blockedAlert_title":"Robot bloqué !","blockedAlert_message":"Veuillez enlever l'obstacle ou me déplacer","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Aller en station de charge ?","lowBattery_title":"Batterie faible","lowBattery_message":"Aller en station de charge ?","errorlaunchAlert_title":"Une erreur est survenue","errorlaunchAlert_message":"Veuillez rententer de lancer la ronde","robotmuststayondocking_title":"Batterie faible","robotmuststayondocking_message":"Le robot doit rester sur la docking","errorNavAlert_title":"Une erreur est survenue","errorNavAlert_message":"Veuillez appeler le support technique si le problème persiste","lostAlert_title":"Robot perdu !","lostAlert_message":"Veuillez appeler le support technique","quitConfirm_title":"Quitter l'application ?","errorBlocked_title":"Robot bloqué","errorBlocked_message":"Veuillez vérifier que le robot est bien dégagé","statusRedPresent_title":"Erreur","statusRedPresent_message":"Veuillez me redémarrer OU appeler mon fabricant si le problème persiste","statusGreenPresent_title":"Statut vert","statusGreenPresent_message":"Le robot est en bonne santé !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Sortir de la station de charge ?","leaveDockingAlert_title":"Robot en charge","leaveDockingAlert_message":"Veuillez me retirer de la station de charge","askHelp_title":"Aide demandée","askHelp_message":"Quelqu'un va venir vous aider","walk":"Marchez !","password":"Mot de passe","enterPassword":"Nécessaire pour accéder aux paramètres.","wrongPassword":"Mot de passe incorrect. Veuillez réessayer.","chosenfreemode":"Bonjour ! Vous avez choisi la marche libre ! Choisissez votre vitesse et la taille de la zone de marche.","letsstart":"Appuyez sur Allons-y quand vous êtes prêt à marcher !","wantstop":"Vous voulez faire une pause ? On continue plus tard !","URL_assistancemobility":"assets/pdf/tuto_FR.pdf","rgpd_txt":"Conformément à la loi du 6 janvier 1978 modifiée et au « Règlement Général de Protection des Données », vos données personnelles de contact sont collectées afin que vous puissiez recevoir les alertes du robot Kompai (personne détectée, robot bloqué, batterie faible..).  Vos données seront traitées par Korian et Kompai pendant la durée requise pour l’utilisation de la plateforme. Vous pouvez, notamment, vous opposez au traitement de vos données,  obtenir une copie de vos données, les rectifier ou les supprimer en écrivant au DPO et en justifiant votre identité à l’adresse : rgpd@kompai.com ou KOMPAI Robotics, Technopole d'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(61);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AlertService = /** @class */ (function () {
    function AlertService(app, param, http) {
        this.app = app;
        this.param = param;
        this.http = http;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        };
    }
    AlertService.prototype.ngOnInit = function () {
    };
    AlertService.prototype.handleError = function (error) {
        console.log('Error: ', error);
    };
    // function send mail without image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    AlertService.prototype.sendmailPerso = function (bod, suj, dest, data, usern, passw, robotn) {
        var _this = this;
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn
        };
        var link = "http://localhost/ionicDB/SendMail/sendmail.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
                //console.log(this.param.durationNS);
                if (response == "OK") {
                    var s = _this.param.serialnumber + ' : Duration App';
                    if (suj == s) {
                        _this.param.updateDurationNS(_this.param.durationNS[0].id_duration);
                        _this.param.durationNS.shift();
                        //console.log(this.param.durationNS);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertData = function (bod, suj) {
        this.sendmailPerso(bod, suj, this.param.datamaillist, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertClient = function (bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailPerso(bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    AlertService.prototype.checkwifi = function (bool) {
        this.allowmail = bool;
    };
    // function send mail with image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    // url : image attachment
    AlertService.prototype.sendmailImgperso = function (url, bod, suj, dest, data, usern, passw, robotn) {
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn,
            url: url
        };
        var link = "http://localhost/ionicDB/SendMail/sendmailImg.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
            });
        }
    };
    AlertService.prototype.sendAlertImgData = function (url, bod, suj) {
        this.sendmailImgperso(url, bod, suj, this.param.datamail, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertImgClient = function (url, bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailImgperso(url, bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send sms
    AlertService.prototype.sendsmsperso = function (mess, num) {
        messageAenvoyer = mess;
        numTelToElement = num;
        envoyerSMS();
    };
    AlertService.prototype.appError = function (info) {
        this.sendAlertData('<br> An error occurred, the round application has been reload automatically' +
            '<br> Code alert : 1' +
            info, this.param.serialnumber + ' : Error (App Mobility assistance)');
    };
    AlertService.prototype.charging = function (info) {
        this.sendAlertData('<br> The robot has docked successfully' +
            '<br> Code alert : 2' +
            info, this.param.serialnumber + ' : Charging');
    };
    AlertService.prototype.noLongerBlocked = function (info) {
        this.sendAlertData('<br> The robot is no longer blocked' +
            '<br> Code alert : 3' +
            info, this.param.serialnumber + ' : Automatic release');
    };
    AlertService.prototype.manualintervention = function (info) {
        this.sendAlertData('<br> Someone unlocked the robot' +
            '<br> Code alert : 25' +
            info, this.param.serialnumber + ' : Manual release');
    };
    AlertService.prototype.appOpen = function (info) {
        this.sendAlertData('<br> Someone oppened the walkassist app.' +
            '<br> Code alert : 24' +
            info, this.param.serialnumber + ' : App opened (WalkAssist)');
    };
    AlertService.prototype.duration = function (olddata, info) {
        console.log("duration alert");
        this.sendAlertData('<br> Here is a count of the duration of use of the apps in minutes' +
            '<br> Date duration : ' + olddata.date +
            '<br> Round duration : ' + olddata.round +
            '<br> Patrol duration : ' + olddata.patrol +
            '<br> Walk duration : ' + olddata.walk +
            '<br> Battery duration : ' + olddata.battery +
            '<br> Toolbox duration : ' + olddata.toolbox +
            '<br> Logistic duration : ' + olddata.logistic +
            '<br> Code alert : 26' +
            info, this.param.serialnumber + ' : Duration App');
    };
    AlertService.prototype.naverror = function (info) {
        this.sendAlertData('<br> A navigation error has occurred. The robot may be lost. The nominal current may have been exceeded.' +
            '<br> Code alert : 4' +
            info, this.param.serialnumber + ' : Navigation error');
    };
    AlertService.prototype.lowBattery = function (info) {
        this.sendAlertData('<br> The robot must be sent to the docking station.' +
            '<br> Code alert : 5' +
            info, this.param.serialnumber + ' : Battery Critical');
    };
    AlertService.prototype.errorblocked = function (info) {
        this.sendAlertData('<br> The robot has been stopped because the rated current is exceeded' +
            '<br> Code alert : 6' +
            info, this.param.serialnumber + ' : High current');
    };
    AlertService.prototype.robotLost = function (info) {
        this.sendAlertData('<br> The robot is lost. It must be relocated' +
            '<br> Code alert : 7' +
            info, this.param.serialnumber + ' : Robot Lost');
    };
    AlertService.prototype.blockingdocking = function (info) {
        this.sendAlertData('<br> The robot encountered an obstacle while heading for the docking station' +
            '<br> Code alert : 8' +
            info, this.param.serialnumber + " : Can't reach docking");
    };
    AlertService.prototype.mobAssistSOS = function (info) {
        this.sendAlertData('<br> Somebody is asking for help' +
            '<br> Code alert : 9' +
            info, this.param.serialnumber + ' : SOS');
    };
    AlertService.prototype.mobAssistActivated = function (info) {
        this.sendAlertData('<br> Somebody starts walking with Kompai' +
            '<br> Code alert : 22' +
            info, this.param.serialnumber + ' : Mobility assistance activated');
    };
    AlertService.prototype.appInUse = function (info) {
        this.sendAlertData('<br> Somebody is walking with Kompai' +
            '<br> Code alert : 23' +
            info, this.param.serialnumber + ' : Mobility assistance in use');
    };
    AlertService.prototype.displayTuto = function (info) {
        this.sendAlertData('<br> Someone is reading the tuto' +
            '<br> Code alert : 10' +
            info, this.param.serialnumber + ' : Tuto opened');
    };
    AlertService.prototype.leaveDocking = function (info) {
        this.sendAlertData('<br> Kompai is no longer on docking' +
            '<br> Code alert : 11' +
            info, this.param.serialnumber + ' : Leave docking');
    };
    AlertService.prototype.askCharge = function (info) {
        this.sendAlertData('<br> The robot must move towards docking' +
            '<br> Code alert : 12' +
            info, this.param.serialnumber + ' : Charging request');
    };
    AlertService.prototype.appClosed = function (info) {
        this.sendAlertData('<br> Somebody has closed the round application' +
            '<br> Code alert : 13' +
            info, this.param.serialnumber + ' : App Mobility assistance closed');
    };
    AlertService.prototype.blocking = function (info) {
        this.sendAlertData('<br> The robot is blocked by an obstacle' +
            '<br> Code alert : 19' +
            info, this.param.serialnumber + ' : Blocking');
    };
    AlertService.prototype.SOS = function (info) {
        this.sendAlertData('<br> Somebody is asking for help' +
            '<br> Code alert : 9' +
            info, this.param.serialnumber + ' : SOS');
    };
    /////////////////////////////////// Mail client
    AlertService.prototype.SOS_c = function () {
        this.sendAlertClient(this.param.datatext.mailSOS_body, this.param.datatext.mailSOS_suj);
    };
    AlertService.prototype.Battery_c = function () {
        this.sendAlertClient(this.param.datatext.mailBattery_body, this.param.datatext.mailBattery_suj);
    };
    AlertService.prototype.Blocked_c = function (url) {
        this.sendAlertImgClient(url, this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    AlertService.prototype.Block_c = function () {
        this.sendAlertClient(this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('video'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "canvas", void 0);
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2__param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 310:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","round":"ENTERTAINMENT TOUR","patrol":"Patrol","patrolInProgress":"PATROLLING","roundInProgress":"ENTERTAINMENT TOUR IN PROGRESS","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","walkInProgress":"WALK AT YOUR OWN PACE WITHOUT PUSHING","notpush":"INSTALL YOURSELF AS IN THE PHOTO AND PRESS «LET'S GO»","mails":"Emails","sms":"SMS","langage":"Language","distance_covered":"Distance travelled","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","sentinel":"Sentry","morningRound":"Morning tour","eveningRound":"Evening tour","btn_go":"GO","btn_help":"Help","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","deletemail":"Please delete an address first","mailadd":"Email address added!","mailincorrect":"Invalid email address","newmail":"New address","cantstart":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","btn_save":"Save","choose_speed":"Choose speed","speed":"Speed","receivepic":"Receive pictures","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","gobehindrobot":"MOVE INTO THE WALK AREA","choosedestination":"CHOOSE DESTINATION AND PRESS «LET'S GO»","chooseround":"CHOOSE THE ROUND AND PRESS «LET'S GO»","gobackward":"PLEASE BACK UP, YOU ARE TOO CLOSE TO THE ROBOT","wait":"WAIT FOR THE END OF THE COUNT BEFORE WALKING","stepback":"Please step back slightly.","closer":"Please get closer to the robot.","choose_walkzone":"Edit walk size","walkzone":"Walk size","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"FALL detected","mailPerson_suj":"Person detected","mailPerson_body":"The robot detected a person <br>","mailRemove":"Deletion done","presentAlert_title":"Robot in charge","presentAlert_message":"I'm going to back up !","AlertConnectedToI":"The robot is connected to Internet","presentConfirm_title":"Round Suspended","presentConfirm_message":"Continue the round ?","FallConfirm_title":"Fall detected !","FallConfirm_message":"Continue the patrol ?","RemoteConfirm_title":"Remote stop round","RemoteConfirm_message":"Continue the round ?","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Go to the charging station ?","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","walk":"Walk !","password":"Password","enterPassword":"Required to access settings.","wrongPassword":"Wrong password. Please try again.","chosenfreemode":"Great! You have choosen Free mode. You can walk freely and you will have to turn the steer yourself.","letsstart":"Let’s start today’s session! Press “Let’s go” when you are ready to start.","wantstop":"I see you want to stop the exercise. Ok, we can continue later!","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In accordance with the amended law of 6 January 1978 and the «General Data Protection Regulations», your personal contact data are collected so that you can receive alerts from the Kompai robot (person detected, robot blocked, low battery, etc.). Your data will be processed by Korian and Kompai for the time required to use the platform. You can, in particular, oppose the processing of your data, obtain a copy of your data, rectify or delete them by writing to the DPO and justifying your identity at: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 311:
/***/ (function(module, exports) {

module.exports = {"quit":"SALIR","battery":"BATERÍA","return":"REGRESAR","tutorial":"TUTORIAL","param":"PARÁMETROS","receivesms":"RECIBIR ALERTAS POR SMS","receivemail":"RECIBIR ALERTAS POR CORREO ELECTRÓNICO","changelangage":"CAMBIAR EL IDIOMA","godocking":"EN CAMINO A LA ESTACÍON DE CARGA","round":"RECORRIDO DE ENTRETENIMIENTO","patrol":"Patrulla","patrolInProgress":"PATRULLANDO","roundInProgress":"RECORRIDO DE ENTRETENIMIENTO EN CURSO","moving":"EN MOVIMIENTO PARA :","charging_remaining":"ROBOT EN CARGA - BATERÍA: ","walkInProgress":"CAMINE A SU PROPIO RITMO SIN EMPUJAR","notpush":"COLÓQUESE COMO EN LA FOTO Y PRESIONE «EMPAZAR»","mails":"Correos electrónicos","sms":"SMS","langage":"Idioma","distance_covered":"Distancia recorrida","goto":"Ir a :","inProgress":"EN CURSO","post":"Posición","sentinel":"Centinela","morningRound":"Ronda de la mañana","eveningRound":"Ronda de la noche","btn_go":"INICIAR","btn_help":"Ayuda","btn_walk":"Empezar","btn_stop":"ALTO","btn_charge":"CARGAR","btn_cancel":"Cancelar","btn_ok":"OK","btn_yes":"SÍ","btn_no":"NO","numexist":"¡ Este número ya existe !","deletenum":"Por favor, borre un número primero","numadd":"Número añadido","numincorrect":"Número inválido","newnum":"Nuevo número","mailexist":"¡ Esta dirección de correo electrónico ya existe ! ","deletemail":"Por favor, elimine primero una dirección de correo electrónico","mailadd":"¡ Dirección de correo electrónico añadida !","mailincorrect":"Dirección de correo electrónico inválida","newmail":"Nueva dirección de correo electrónico","error":"Error","cantstart":"No se puede iniciar la aplicación. Compruebe si el robot está bien encendido.","nointernet":"Sin acceso a internet","btn_save":"Guardar","choose_speed":"Elige la velocidad","speed":"Velocidad","receivepic":"Recibir fotos","editpswd":"EDITAR CONTRASEÑA","currentpswd":"Contraseña actual :","newpswd":"Nueva contraseña :","save":"Guardar","success":"Está hecho !","pswdsaved":"Contraseña guardada","langage1":"Idioma","editpswd1":"Editar contraseña","wrongpass":"Contraseña incorrecta","gobehindrobot":"COLOCA A LA ZONA DE PASEO","choosedestination":"ELIGE EL DESTINO Y HAZ CLICK EN «EMPEZAR»","chooseround":"ELIGE LA RONDA Y HAZ CLICK EN «EMPEZAR»","gobackward":"POR FAVOR, ATRÁS, ESTÁS DEMASIADO CERCA DEL ROBOT","wait":"ESPERAR EL FINAL DE LA CUENTA ANTES DE CAMINAR","stepback":"Por favor, retroceda un poco.","closer":"Por favor, acércate al robot.","choose_walkzone":"Cambiar zona de paso","walkzone":"Zona de paseo","smsSOS":"Alguien ha pulsado el botón SOS del robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona necesita ayuda","mailBattery_suj":"Batería Baja","mailBattery_body":"<br> Hay que cargar al robot. Por favor, pongalo en su estación de carga.","mailBlocked_suj":"Robot bloqueado","mailBlocked_body":"<br> El robot está bloqueado <br>","mailFall_body":"El robot detecta a una persona en el suelo <br>","mailFall_suj":"CAÍDA detectada","mailPerson_suj":"Persona detectada","mailPerson_body":"El robot detectó a una persona <br>","mailRemove":"Supresión realizada","presentAlert_title":"Robot en carga","presentAlert_message":"¡ Voy a ir hacia atrás !","AlertConnectedToI":"El robot está conectado a internet","presentConfirm_title":"Ronda suspendida","presentConfirm_message":"¿ Volver a la ronda ?","FallConfirm_title":"¡ Caída detectada !","FallConfirm_message":"¿ Volver a la patrulla ?","RemoteConfirm_title":"Ronda parada a distancia","RemoteConfirm_message":"¿ Volver a la ronda ?","blockedAlert_title":"¡ Robot bloqueado !","blockedAlert_message":"Por favor, quite el obstáculo o muevame","goDockingConfirm_title":"Batería: ","goDockingConfirm_message":"¿ Ir a la estación de carga ?","lowBattery_title":"Batería Baja","lowBattery_message":"¿ Ir a la estación de carga ?","errorlaunchAlert_title":"Se ha producido un error","errorlaunchAlert_message":"Intente volver a empezar la ronda, por favor","robotmuststayondocking_title":"Batería Baja","robotmuststayondocking_message":"El robot debe quedarse en el atraque","errorNavAlert_title":"Se ha producido un error","errorNavAlert_message":"Por favor, llame al soporte técnico si el problema persiste","lostAlert_title":"¡ Robot perdido !","lostAlert_message":"Por favor, llame al soporte técnico","quitConfirm_title":"¿ Salir de la aplicación ?","errorBlocked_title":"Robot bloqueado","errorBlocked_message":"Compruebe si el robot está desbloqueado","statusRedPresent_title":"Error","statusRedPresent_message":"Por favor, reinicie me o llame a mi fabricante si el problema persiste","statusGreenPresent_title":"Situación verde","statusGreenPresent_message":"¡ El robot está sano !","leaveDockingConfirm_title":"Batería: ","leaveDockingConfirm_message":"¿ Salir de la estación de carga ?","leaveDockingAlert_title":"Robot cargando","leaveDockingAlert_message":"Por favor, retireme de la estación de carga","askHelp_title":"Ayuda solicitada","askHelp_message":"Alguien vendrá a ayudarte","walk":"Ande","password":"Contraseña","enterPassword":"Requerido para acceder a la configuración.","wrongPassword":"Contraseña incorrecta. Por favor, intente de nuevo.","chosenfreemode":"¡Genial! Ha elegido la modalidad libre. Puede caminar hacia donde quiera y deberá girar usted en la dirección que desee","letsstart":"¡Vamos a empezar la sesión de hoy! Pulse “Empezar” cuando esté. ","wantstop":"Veo que no quiere seguir. De acuerdo, podemos continuar más tarde","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"De acuerdo con la ley modificada del 6 de enero de 1978 y el «Reglamento general de protección de datos», sus datos de contacto personales se recopilan para que pueda recibir alertas del robot Kompai (persona detectada, robot bloqueado, batería baja, etc.). Korian y Kompai procesarán sus datos durante el tiempo necesario para utilizar la plataforma. Puede, en particular, oponerse al tratamiento de sus datos, obtener una copia de sus datos, rectificarlos o suprimirlos escribiendo al DPO y justificando su identidad en: rgpd@kompai.com o KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - Francia."}

/***/ }),

/***/ 312:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","round":"ENTERTAINMENT TOUR","patrol":"Patrol","patrolInProgress":"PATROLLING","roundInProgress":"ENTERTAINMENT TOUR IN PROGRESS","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","walkInProgress":"WALK AT YOUR OWN PACE WITHOUT PUSHING","notpush":"INSTALL YOURSELF AS IN THE PHOTO AND PRESS «LET'S GO»","mails":"Emails","sms":"SMS","langage":"Language","distance_covered":"Distance travelled","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","sentinel":"Sentry","morningRound":"Morning tour","eveningRound":"Evening tour","btn_go":"GO","btn_help":"Help","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","deletemail":"Please delete an address first","mailadd":"Email address added!","mailincorrect":"Invalid email address","newmail":"New address","cantstart":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","btn_save":"Save","choose_speed":"Choose speed","speed":"Speed","receivepic":"Fotos erhalten","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","gobehindrobot":"POSITION YOURSELF CORRECTLY BEHIND THE ROBOT","choosedestination":"CHOOSE DESTINATION AND PRESS «LET'S GO»","chooseround":"CHOOSE THE ROUND AND PRESS «LET'S GO»","gobackward":"PLEASE BACK UP, YOU ARE TOO CLOSE TO THE ROBOT","wait":"WAIT FOR THE END OF THE COUNT BEFORE WALKING","stepback":"Please step back slightly.","closer":"Please get closer to the robot.","choose_walkzone":"Edit walk size","walkzone":"Walk size","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"FALL detected","mailPerson_suj":"Person detected","mailPerson_body":"The robot detected a person <br>","mailRemove":"Deletion done","presentAlert_title":"Robot in charge","presentAlert_message":"I'm going to back up !","AlertConnectedToI":"The robot is connected to Internet","presentConfirm_title":"Round Suspended","presentConfirm_message":"Continue the round ?","FallConfirm_title":"Fall detected !","FallConfirm_message":"Continue the patrol ?","RemoteConfirm_title":"Remote stop round","RemoteConfirm_message":"Continue the round ?","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Go to the charging station ?","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","walk":"Walk !","password":"Password","enterPassword":"Required to access settings.","wrongPassword":"Wrong password. Please try again.","chosenfreemode":" ","letsstart":" ","wantstop":" ","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In accordance with the amended law of 6 January 1978 and the «General Data Protection Regulations», your personal contact data are collected so that you can receive alerts from the Kompai robot (person detected, robot blocked, low battery, etc.). Your data will be processed by Korian and Kompai for the time required to use the platform. You can, in particular, oppose the processing of your data, obtain a copy of your data, rectify or delete them by writing to the DPO and justifying your identity at: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 313:
/***/ (function(module, exports) {

module.exports = {"quit":"ΕΞΟΔΟΣ","battery":"ΜΠΑΤΑΡΙΑ","return":"ΕΠΙΣΤΡΟΦΗ","tutorial":"ΟΔΗΓΙΕΣ","param":"ΡΥΘΜΙΣΕΙΣ","receivesms":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ ΜΗΝΥΜΑΤΟΣ","receivemail":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ EMAIL","changelangage":"ΑΛΛΑΓΗ ΓΛΩΣΣΑΣ","godocking":"ΕΠΙΣΤΡΟΦΗ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","moving":"ΣΕ ΔΙΑΔΙΚΑΣΙΑ ΜΕΤΑΚΙΝΗΣΗΣ: ΠΡΟΣ","charging_remaining":"ΡΟΜΠΟΤ ΣΕ ΦΟΡΤΙΣΗ - ΜΠΑΤΑΡΙΑ: ","walkInProgress":"ΠΕΡΠΑΤΗΣΤΕ ΣΤΟ ΔΙΚΟ ΣΑΣ ΡΥΘΜΟ ΧΩΡΙΣ ΝΑ ΣΠΡΩΧΝΕΤΕ","notpush":"ΣΤΑΘΕΙΤΕ ΟΠΩΣ ΣΤΗ ΦΩΤΟΓΡΑΦΙΑ ΚΑΙ ΕΠΙΛΕΞΤΕ «ΠΑΜΕ»","mails":"Emails","sms":"ΜΗΝΥΜΑΤΑ","langage":"Γλώσσα","distance_covered":"Απόσταση που διανύθηκε","goto":"Πάμε:","inProgress":"ΣΕ ΕΞΕΛΙΞΗ","post":"Θέση","btn_go":"ΠΑΜΕ","btn_help":"Βοήθεια","btn_walk":"ΠΑΜΕ","btn_stop":"ΠΑΥΣΗ","btn_charge":"ΦΟΡΤΩΣΗ","btn_cancel":"Ακύρωση","btn_ok":"OK","btn_yes":"ΝΑΙ","btn_no":"ΟΧΙ","numexist":"Αυτός ο αριθμός τηλεφώνου υπάρχει ήδη!","deletenum":"Παρακαλώ διαγράψτε έναν αριθμό τηλεφώνου πρώτα","numadd":"Ο αριθμός τηλεφώνου προστέθηκε","numincorrect":"Μη έγκυρος αριθμός τηλεφώνου","newnum":"Νέος αριθμός τηλεφώνου","mailexist":"Αυτή η διεύθυνση email υπάρχει ήδη! ","deletemail":"Παρακαλούμε διαγράψτε μία διεύθυνση email πρώτα","mailadd":"Η διεύθυνση email προστέθηκε!","mailincorrect":"Άκυρη διεύθυνση email","newmail":"Νέα διεύθυνση email","cantstart":"Η φόρτωση της εφαρμογής απέτυχε. Ελέγχξτε ότι το ρομπότ έχει ενεργοποιηθεί σωστά.","nointernet":"Δεν υπάρχει σύνδεση στο διαδίκτυο","btn_save":"Αποθήκευση","choose_speed":"Επιλογή ταχύτητας","speed":"Ταχύτητα","receivepic":"Λήψη φωτογραφιών","editpswd":"ΕΠΕΞΕΡΓΑΣΙΑ ΚΩΔΙΚΟΥ ΠΡΟΣΒΑΣΗΣ","currentpswd":"Τρέχων κωδικός πρόσβασης :","newpswd":"Νέος κωδικός πρόσβασης :","save":"Αποθήκευση","success":"Επιτυχία","pswdsaved":"Ο κωδικός πρόσβασης αποθηκεύτηκε","langage1":"Γλώσσα","editpswd1":"Επεξεργασία κωδικού πρόσβασης","wrongpass":"Λάθος κωδικός πρόσβασης","gobehindrobot":"ΠΡΟΧΩΡΗΣΤΕ ΣΤΗΝ ΚΑΤΑΛΛΗΛΗ ΠΕΡΙΟΧΗ ΓΙΑ ΒΑΔΙΣΗ","choosedestination":"ΕΠΙΛΕΞΤΕ ΠΡΟΟΡΙΣΜΟ ΚΑΙ ΠΑΤΗΣΤΕ «ΠΑΜΕ»","chooseround":"ΕΠΙΛΕΞΤΕ ΤΗ ΔΙΑΔΡΟΜΗ ΚΑΙ ΠΑΤΗΣΤΕ «ΠΑΜΕ»","gobackward":"ΠΑΡΑΚΑΛΩ ΜΕΤΑΚΙΝΗΘΕΙΤΕ ΠΙΣΩ, ΕΙΣΤΕ ΠΟΛΥ ΚΟΝΤΑ ΣΤΟ ΡΟΜΠΟΤ","wait":"ΠΕΡΙΜΕΝΕΤΕ ΜΕΧΡΙ ΤΟ ΤΕΛΟΣ ΤΗΣ ΑΝΤΙΣΤΡΟΦΗΣ ΜΕΤΡΗΣΗΣ ΠΡΙΝ ΞΕΚΙΝΗΣΕΤΕ ΤΗ ΒΑΔΙΣΗ","stepback":"Παρακαλώ μετακινηθείτε λίγο πίσω.","closer":"Παρακαλώ πλησιάστε το ρομπότ.","choose_walkzone":"Επεξεργαστείτε την απόσταση της διαδρομής","walkzone":"Απόσταση διαδρομής","smsSOS":"Κάποιος πάτησε το κουμπί κινδύνου του ρομπότ","mailSOS_suj":"ΚΙΝΔΥΝΟΣ","mailSOS_body":"<br> Κάποιος χρειάζεται βοήθεια","mailBattery_suj":"Χαμηλή μπαταρία","mailBattery_body":"<br> Το ρομπότ χρειάζεται φόρτιση. Παρακαλούμε τοποθετήστε το στο σταθμό φόρτισης.","mailBlocked_suj":"Μπλοκαρισμένο ρομπότ","mailBlocked_body":"<br> Tο ρομπότ έχει μπλοκαριστεί <br>","mailFall_body":"Το ρομπότ ανιχνεύει κάποιο άτομο στην πορεία <br>","mailFall_suj":"Ανιχνεύφθηκε ΠΤΩΣΗ","mailPerson_suj":"Ανιχνεύφθηκε άτομο","mailPerson_body":"Το ρομπότ ανίχνευσε κάποιο άτομο <br>","mailRemove":"Η διαγραφή πραγματοποιήθηκε","presentAlert_title":"Το ρομπότ είναι σε κατάσταση φόρτισης","presentAlert_message":"Θα κρατήσω αντίγραφα ασφαλείας !","AlertConnectedToI":"Το ρομπότ είναι συνδεδεμένο στο διαδίκτυο","presentConfirm_title":"Διακοπή διαδρομής","presentConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή;","FallConfirm_title":"Ανιχνεύφθηκε πτώση !","FallConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή;","RemoteConfirm_title":"Απομακρυσμένη διακοπή της διαδρομής","RemoteConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή ;","blockedAlert_title":"Το ρομπότ έχει μπλοκαριστεί !","blockedAlert_message":"Παρακαλώ αφαιρέστε το εμπόδιο ή μετακινήστε με","goDockingConfirm_title":"Μπαταρία: ","goDockingConfirm_message":"Θέλετε να πάω στο σταθμό φόρτισης ;","lowBattery_title":"Χαμηλή μπαταρία","lowBattery_message":"Μετάβαση στο σταθμό φόρτισης ?","errorlaunchAlert_title":"Προέκυψε κάποιο σφάλμα","errorlaunchAlert_message":"Παρακαλώ προσπαθήστε ξανά για να ξεκινήσετε τη διαδρομή","robotmuststayondocking_title":"Χαμηλή μπαταρία","robotmuststayondocking_message":"Το ρομπότ πρέπει να μείνει στο σταθμό φόρτισης","errorNavAlert_title":"Προέκυψε κάποιο σφάλμα","errorNavAlert_message":"Παρακαλώ καλέστε την τεχνική υποστήριξη σε περίπτωση που προκύψει κάποιο πρόβλημα","lostAlert_title":"Το ρομπότ χάθηκε !","lostAlert_message":"Παρακαλώ καλέστε την τεχνική υποστήριξη","quitConfirm_title":"Θέλετε να βγείτε από την εφαρμογή ;","errorBlocked_title":"Το ρομπότ έχει μπλοκαριστεί","errorBlocked_message":"Παρακαλούμε ελέγξτε εάν εμποδίζει κάτι το ρομπότ","statusRedPresent_title":"Σφάλμα","statusRedPresent_message":"Παρακαλώ επανεκκινήστε το ρομπότ ή καλέστε τον κατασκευαστή αν το πρόβλημα παραμένει","statusGreenPresent_title":"Κατάσταση ενεργή","statusGreenPresent_message":"Το ρομπότ είναι υγιές !","leaveDockingConfirm_title":"Μπαταρία: ","leaveDockingConfirm_message":"Θέλετε να φύγω από το σταθμό φόρτισης;","leaveDockingAlert_title":"Το ρομπότ είναι σε κατάσταση φόρτισης","leaveDockingAlert_message":"Παρακαλώ, απομακρύνετέ με από το σταθμό φόρτισης","askHelp_title":"Ζητήθηκε βοήθεια","askHelp_message":"Κάποιος θα έρθει να σας βοηθήσει","walk":"Περπατήστε !","password":"Κωδικός πρόσβασης","enterPassword":"Απαιτείται για να αποκτήσετε πρόσβαση στις ρυθμίσεις.","wrongPassword":"Λάθος κωδικός πρόσβασης. Παρακαλώ προσπαθήστε ξανά.","chosenfreemode":" Ωραία! Έχετε επιλέξει τη λειτουργία ελεύθερης βάδισης. Μπορείτε να περπατήσετε ελεύθερα και πρέπει να μετακινηθείτε μόνοι σας.","letsstart":"Ας ξεκινήσουμε τη σημερινή συνεδρία! Επιλέξτε “Πάμε” όταν είστε έτοιμοι να ξεκινήσετε.","wantstop":"Φαίνεται ότι θέλετε να σταματήσετε τη διαδρομή. ΟΚ, θα συνεχίσουμε αργότερα!","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"Σύμφωνα με το νόμο που καθορίστηκε στις 6 Ιανουαρίου 1978 και τον «Γενικό Κανονισμό για την Προστασία των Δεδομένων», τα προσωπικά σας δεδομένα συλλέγονται για να μπορείτε να λάβετε ειδοποιήσεις από το ρομπότ Kompai (ανίχνευση ατόμου, μπλοκάρισμα ρομπότ, χαμηλή μπαταρία κ.λπ.). Τα δεδομένα σας θα επεξεργαστούν από την Korian και την Kompai για το χρόνο που απαιτείται για τη χρήση της πλατφόρμας. Συγκεκριμένα, μπορείτε να ανακαλέσετε την επεξεργασία των δεδομένων σας, να λάβετε ένα αντίγραφο των δεδομένων σας, να αποσύρετε ή να διαγράψετε τα δεδομένα σας, αφού απενθυθείτε στον Υπεύθυνο Προστασίας Δεδομένων και ορίσετε την ταυτότητά σας στο rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 325:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 327:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 347:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 349:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(12);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PopupService = /** @class */ (function () {
    function PopupService(app, alert, param, api, alertCtrl, toastCtrl) {
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    PopupService.prototype.onSomethingHappened1 = function (fn) {
        this.accessparam = fn;
    };
    PopupService.prototype.onSomethingHappened2 = function (fn) {
        this.addMail = fn;
    };
    PopupService.prototype.ngOnInit = function () { };
    PopupService.prototype.startFailedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.cantstart,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.leaveDockingAlert = function () {
        // pop up if the robot is in the docking when you start the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingAlert_title,
            message: this.param.datatext.leaveDockingAlert_message,
            cssClass: "alertstyle",
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorNavAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorNavAlert_title,
            message: this.param.datatext.errorNavAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.blockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.blockedAlert_title,
            message: this.param.datatext.blockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.quitConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.quitConfirm_title,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        //console.log('Oui clicked');
                        _this.api.close_app = true;
                        _this.alert.appClosed(_this.api.mailAddInformationBasic());
                        //stop the round and quit the app
                        _this.api.abortNavHttp();
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.lowBattery = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.lowBattery_title,
            message: this.param.datatext.lowBattery_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.towardDocking = true;
                        //console.log(this.api.towardDocking=true);
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorBlocked = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.errorBlocked_title,
            message: this.param.datatext.errorBlocked_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('OK clicked');
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.lostAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.lostAlert_title,
            message: this.param.datatext.lostAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.robotmuststayondocking = function () {
        // pop up if the robot is in the docking when you start the round
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.robotmuststayondocking_title,
            message: this.param.datatext.robotmuststayondocking_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.goDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must go to the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.goDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.goDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                        _this.api.walkercmdstartdetect();
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        //console.log('Oui clicked');
                        _this.api.towardDocking = true;
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusRedPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.statusRedPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusGreenPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusGreenPresent_title,
            message: this.param.datatext.statusGreenPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askHelp = function () {
        // pop up displayed when someone push the sos btn
        var alert = this.alertCtrl.create({
            title: this.param.datatext.askHelp_title,
            message: this.param.datatext.askHelp_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.leaveDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must leave the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.leaveDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.disconnectHttp();
                        _this.alert.leaveDocking(_this.api.mailAddInformationBasic());
                        setTimeout(function () {
                            _this.api.walkercmdstartdetect();
                        }, 8000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswd = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPassword = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.displayrgpd = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: "RGPD",
            message: this.param.datatext.rgpd_txt,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        _this.addMail(ev);
                    }
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.showToast = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "csstoast"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PopupService);
    return PopupService;
}());

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ 364:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpeechService = /** @class */ (function () {
    function SpeechService(param) {
        this.param = param;
        this.msg = new SpeechSynthesisUtterance();
        this.msg.volume = parseFloat("1");
        this.msg.rate = parseFloat("1");
        this.msg.pitch = parseFloat("1");
        this.synth = window.speechSynthesis;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
    }
    SpeechService.prototype.getVoice = function () {
        var _this = this;
        var voices = speechSynthesis
            .getVoices()
            .filter(function (voice) {
            return voice.localService == true && voice.lang == _this.param.langage;
        });
        if (voices.length > 1) {
            this.msg.voice = voices[1];
        }
        else {
            this.msg.voice = voices[0];
        }
    };
    // Create a new utterance for the specified text and add it to
    // the queue.
    SpeechService.prototype.speak = function (text) {
        if (this.param.allowspeech == 1) {
            if (text != " ") {
                this.synth.cancel();
                this.msg.lang = this.param.langage;
                // Create a new instance of SpeechSynthesisUtterance.
                // Set the text.
                this.getVoice();
                this.msg.text = text;
                //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
                //console.log(msg);
                // Queue this utterance.
                this.synth.speak(this.msg);
            }
            console.log(this.msg);
        }
    };
    SpeechService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__param_service__["a" /* ParamService */]])
    ], SpeechService);
    return SpeechService;
}());

//# sourceMappingURL=speech.service.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FreeAssistancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alert_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popup_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_speech_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FreeAssistancePage = /** @class */ (function () {
    function FreeAssistancePage(speech, popup, alert, param, navCtrl, api, renderer) {
        var _this = this;
        this.speech = speech;
        this.popup = popup;
        this.alert = alert;
        this.param = param;
        this.navCtrl = navCtrl;
        this.api = api;
        this.renderer = renderer;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.startPage = __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__["a" /* StartAssistancePage */];
        this.cptdetected = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
        this.api.metercovered = 0;
        this.updateMeter = setInterval(function () { return _this.getUpdate(); }, 500); // update meter and walker detection every 1/2 secondes
        this.cptduration = setInterval(function () {
            return _this.mail();
        }, 120000); // toutes les 2 min actualise time
        this.api.inUse = true;
        this.is_sendmail = false;
        this.cpt_locked = 0;
        this.is_blocked = false;
        this.timecountdown = 3;
        // declecnche la popup de permission camera si besoin
        this.setupconstraint();
        this.firstcount = false;
        this.selectOptions = {
            title: this.param.datatext.choose_speed,
        };
        this.selectOptionsWalkZone = {
            title: this.param.datatext.choose_walkzone,
        };
    }
    FreeAssistancePage.prototype.mail = function () {
        this.param.cptDuration(); //update duration
        if (this.param.durationNS.length > 1) {
            if (this.api.wifiok) {
                this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
            }
        }
    };
    FreeAssistancePage.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.speech.speak(_this.param.datatext.chosenfreemode);
        }, 1000);
        setTimeout(function () {
            if (!_this.api.inUse) {
                _this.speech.speak(_this.param.datatext.letsstart);
            }
        }, 12000);
        this.api.walkercmdstartdetect();
    };
    FreeAssistancePage.prototype.triggerAssist = function (ev) {
        // Call preventDefault() to prevent any further handling
        ev.preventDefault();
        this.onclickassist();
    };
    FreeAssistancePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.pageenter = false;
        setTimeout(function () {
            _this.pageenter = true;
        }, 500);
    };
    FreeAssistancePage.prototype.ionViewWillLeave = function () {
        //clearInterval(this.updateMeter);
        //clearInterval(this.cptduration);
    };
    FreeAssistancePage.prototype.getUpdate = function () {
        //console.log(this.api.selectSpeed);
        if (this.api.close_app) {
            clearInterval(this.updateMeter);
            clearInterval(this.cptduration);
        }
        this.updateTrajectory();
        this.watchIfLocked();
        this.getDetect();
        this.getMeter();
    };
    FreeAssistancePage.prototype.watchIfLocked = function () {
        if (this.api.towardDocking) {
            if (this.api.docking_status.status === 2 ||
                this.api.docking_status.status === 3 ||
                (this.api.anticollision_status.forward < 2 &&
                    this.api.anticollision_status.right < 2 &&
                    this.api.anticollision_status.left < 2)) {
                this.cpt_locked = 0;
                if (this.is_blocked) {
                    this.is_blocked = false;
                    this.popup.alert_blocked.dismiss();
                }
            }
            else if (this.api.anticollision_status.forward === 2 ||
                this.api.anticollision_status.right === 2 ||
                this.api.anticollision_status.left === 2) {
                this.cpt_locked += 1;
            }
            if (this.cpt_locked > 40 && !this.is_blocked) {
                this.popup.blockedAlert();
                this.is_blocked = true;
                this.alert.blockingdocking(this.api.mailAddInformationBasic());
            }
            else if (this.cpt_locked === 100 && this.is_blocked) {
                this.captureBlocked();
            }
        }
    };
    FreeAssistancePage.prototype.sendHelp = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.popup.askHelp();
        this.api.walkercmdstartdetect();
        // change the color and logo of the box
        this.api.inUse = false;
        if (!this.is_sendmail) {
            this.is_sendmail = true;
            // for (let num of this.param.phonenumberlist) {
            //   this.alert.sendsmsperso(this.param.datatext.smsSOS, num);
            // }
            // capture l'image et l'envoie via mail
            this.captureSOS();
            setTimeout(function () {
                _this.is_sendmail = false;
            }, 30000);
        }
    };
    FreeAssistancePage.prototype.getDetect = function () {
        if (this.api.walker_states.status === 3) {
            // if (this.api.walker_states.person["X"] > this.api.walker_states.activationDistance + 0.12) {
            //   this.onclickassist();
            // }
            if (this.api.walker_states.person["X"] > this.api.walker_states.activationDistance || !this.api.walker_states.detected) {
                this.api.position_state = 1; //too far
            }
            else {
                this.api.position_state = 2; //in walkzone
            }
            if (!this.api.walker_states.detected) {
                this.cptdetected = this.cptdetected + 1;
            }
            else {
                this.cptdetected = 0;
            }
        }
        // if smart bouton is pressed or the bouton A of the gamepad is pressed
        if (this.cptdetected > 6 ||
            this.api.btnPush ||
            !this.api.is_connected) {
            if (this.api.walker_states.status === 3) {
                this.cptdetected = 0;
                // if (this.firstcount) {
                //   clearInterval(this.intervalcountdown);
                //   this.firstcount = false;
                // }
                //start the detection and stop the enslavement of the legs
                this.api.walkercmdstartdetect();
                // change the color and logo of the box
                this.api.inUse = false;
            }
            if (this.api.towardDocking && this.api.btnPush) {
                this.api.abortNavHttp();
                this.api.towardDocking = false;
                //this.api.walkercmdstartdetect();
            }
            this.api.btnPush = false;
        }
    };
    FreeAssistancePage.prototype.updateTrajectory = function () {
        // listen the round status to allow the robot to go to the next
        //// go docking or go poi in progress
        if (this.api.towardDocking) {
            this.api.appStart = false;
            if (this.api.differential_status.status === 2) {
                // if current is hight
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorBlocked();
                this.alert.errorblocked(this.api.mailAddInformationBasic());
                this.captureBlocked();
            }
            else if (this.api.navigation_status.status === 5) {
                // nav error
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorNavAlert();
                this.alert.naverror(this.api.mailAddInformationBasic());
            }
            else if (this.api.docking_status.status === 3) {
                this.api.walkercmdstopdetect();
                this.api.towardDocking = false;
                this.alert.charging(this.api.mailAddInformationBasic());
                this.api.close_app = true; //close application
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
            else if (this.api.towardDocking &&
                this.api.navigation_status.status === 0 &&
                this.api.docking_status.detected) {
                //connect to the docking when he detect it
                this.api.connectHttp();
            }
        }
    };
    FreeAssistancePage.prototype.onRefreshMeter = function (ev) {
        ev.preventDefault();
        this.api.traveldebut = this.api.statistics_status.totalDistance;
        this.api.metercovered = 0;
    };
    FreeAssistancePage.prototype.getMeter = function () {
        if (this.api.traveldebut > 0) {
            this.api.metercovered =
                this.api.statistics_status.totalDistance - this.api.traveldebut;
        }
        else {
            this.api.traveldebut = this.api.statistics_status.totalDistance;
        }
    };
    FreeAssistancePage.prototype.onclickassist = function () {
        var _this = this;
        this.api.eyesHttp(4);
        if (this.api.towardDocking) {
            this.api.abortNavHttp();
            this.api.towardDocking = false;
            //this.api.walkercmdstartdetect();
        }
        else {
            this.alert.appInUse(this.api.mailAddInformationWalk());
            if (this.api.walker_states.status === 3) {
                this.pageenter = false;
                this.speech.speak(this.param.datatext.wantstop);
                this.api.walkercmdstartdetect();
                // change the color and logo of the box
                this.api.inUse = false;
                setTimeout(function () {
                    _this.pageenter = true;
                }, 600);
            }
            else {
                this.speech.synth.cancel();
                this.api.inUse = true;
                this.startAssistance_page();
            }
        }
    };
    FreeAssistancePage.prototype.startAssistance_page = function () {
        this.navCtrl.push(this.startPage);
        this.api.walkercmdstartdetect();
    };
    // config la variable constraints
    FreeAssistancePage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("USB")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 },
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                _this.startCamera(constraints);
            });
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    // function launch connection camera
    FreeAssistancePage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices
                .getUserMedia(cs)
                .then(this.attachVideo.bind(this))
                .catch(this.handleError);
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    FreeAssistancePage.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    // function qui attache le flux video à la balise video
    FreeAssistancePage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.renderer.setProperty(this.videoElement.nativeElement, "srcObject", stream);
        this.renderer.listen(this.videoElement.nativeElement, "play", function (event) {
            _this.videoHeight = _this.videoElement.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement.nativeElement.videoWidth;
        });
    };
    // function take snapshot and send mail
    FreeAssistancePage.prototype.captureSOS = function () {
        this.alert.SOS_c();
        this.alert.SOS(this.api.mailAddInformationWalk());
    };
    FreeAssistancePage.prototype.captureBlocked = function () {
        if (this.param.robot.send_pic == 1) {
            this.setupconstraint();
            this.renderer.setProperty(this.canvas.nativeElement, "width", this.videoWidth);
            this.renderer.setProperty(this.canvas.nativeElement, "height", this.videoHeight);
            this.canvas.nativeElement
                .getContext("2d")
                .drawImage(this.videoElement.nativeElement, 0, 0);
            var url = this.canvas.nativeElement.toDataURL();
            console.log(url);
            this.alert.Blocked_c(url);
        }
        else {
            this.alert.Block_c();
        }
    };
    FreeAssistancePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FreeAssistancePage.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FreeAssistancePage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], FreeAssistancePage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], FreeAssistancePage.prototype, "select2", void 0);
    FreeAssistancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-freeassistance",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\free_assistance\free_assistance.html"*/'<!-- mobility support html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="free_assistance"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding class="content">\n\n\n\n  <ion-grid class="main_ion_grid">\n\n    <ion-row responsive-sm class="ionrow">\n\n\n\n      <ion-col col-sm-3 no-padding>\n\n\n\n        <ion-item class="itemselect" *ngIf="api.walker_states.status!=3 && !api.towardDocking">\n\n          <ion-label fixed style=" color:white; margin-left: 10px;">{{param.datatext.speed}}</ion-label>\n\n          <ion-select #select1 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n            class="myCustomSelect" [selectOptions]="selectOptions" [(ngModel)]="api.selectSpeed"\n\n            placeholder={{param.datatext.choose_speed}} (mouseup)="onSliderRelease($event,select1)">\n\n            <ion-option [value]=0.1>10 cm/s</ion-option>\n\n            <ion-option [value]=0.2>20 cm/s</ion-option>\n\n            <ion-option [value]=0.3>30 cm/s</ion-option>\n\n            <ion-option [value]=0.4>40 cm/s</ion-option>\n\n            <ion-option [value]=0.5>50 cm/s</ion-option>\n\n            <ion-option [value]=0.6>60 cm/s</ion-option>\n\n            <ion-option [value]=0.7>70 cm/s</ion-option>\n\n\n\n          </ion-select>\n\n        </ion-item>\n\n        <ion-item class="itemselectwalkzone" *ngIf="api.walker_states.status!=3 && !api.towardDocking">\n\n          <ion-label fixed style=" color:white; margin-left: 10px;">{{param.datatext.walkzone}}</ion-label>\n\n          <ion-select #select2 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n          class="myCustomSelect" [selectOptions]="selectOptionsWalkZone" [(ngModel)]="api.selectWalkZone"\n\n            placeholder={{param.datatext.choose_walkzone}} (mouseup)="onSliderRelease($event,select2)">\n\n            <ion-option [value]=0.4>40 cm</ion-option>\n\n            <ion-option [value]=0.45>45 cm</ion-option>\n\n            <ion-option [value]=0.5>50 cm</ion-option>\n\n            <ion-option [value]=0.55>55 cm</ion-option>\n\n            <ion-option [value]=0.6>60 cm</ion-option>\n\n            <ion-option [value]=0.65>65 cm</ion-option>\n\n            <ion-option [value]=0.70>70 cm</ion-option>\n\n            <ion-option [value]=0.75>75 cm</ion-option>\n\n            <ion-option [value]=0.80>80 cm</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n        <!-- the Go button is disabled if the robot is not connected or if it detects no one -->\n\n        <button (mouseup)="triggerAssist($event)" *ngIf="api.walker_states.status!=3 && !api.towardDocking"\n\n          [disabled]="!api.is_connected || api.walker_states.status===3 || !this.pageenter" class="btnwalk">\n\n          <ion-icon color="light" name="walk" class="icon_style">\n\n            <p class="legende">\n\n              {{this.param.datatext.btn_walk}}\n\n            </p>\n\n          </ion-icon>\n\n        </button>\n\n        <button (mouseup)="triggerAssist($event)" *ngIf="api.walker_states.status===3 || api.towardDocking"\n\n          [disabled]="!this.pageenter" class="btnstop">\n\n          <ion-icon color="light" name="hand" class="icon_style">\n\n            <p class="legende">{{this.param.datatext.btn_stop}}</p>\n\n          </ion-icon>\n\n        </button>\n\n\n\n      </ion-col>\n\n\n\n      <ion-col no-padding class="colmeter">\n\n        <div class="container vh-100">\n\n          <div class="d-flex flex-column align-items-center">\n\n            <div class="p-1">\n\n              <video #video class="vid" autoplay style="display:none;"></video>\n\n            </div>\n\n            <div class="p-1">\n\n              <canvas #canvas style="display:none;" class="vid"></canvas>\n\n            </div>\n\n          </div>\n\n        </div>\n\n        <ion-item no-padding no-lines class="item_style">\n\n          <p class="distance_text">\n\n            {{this.param.datatext.distance_covered}}\n\n          </p>\n\n          <p class="distance">\n\n            {{api.metercovered}} m\n\n          </p>\n\n          <button ion-button (mouseup)="onRefreshMeter($event)" clear class="refresh">\n\n            <ion-icon name="refresh" class="refreshicon"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n      </ion-col>\n\n\n\n      <ion-col col-sm-3 no-padding>\n\n        <button (mouseup)="sendHelp($event)" *ngIf="api.walker_states.status!=3" [disabled]="is_sendmail"\n\n          class="btnhelp">\n\n          <ion-icon top color="light" name="call" class="icon_style">\n\n            <p class="legende">{{this.param.datatext.btn_help}}</p>\n\n          </ion-icon>\n\n        </button>\n\n        <button (mouseup)="sendHelp($event)" *ngIf="api.walker_states.status===3" [disabled]="is_sendmail"\n\n          class="btnhelp2">\n\n          <ion-icon top color="light" name="call" class="icon_style">\n\n            <p class="legende">{{this.param.datatext.btn_help}}</p>\n\n          </ion-icon>\n\n        </button>\n\n        <div *ngIf="api.walker_states.status===3 && api.position_state===2" no-padding class="goodpose"></div>\n\n        <div *ngIf="api.walker_states.status===3 && api.position_state===1" no-padding class="badpose"></div>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\assistancemobilite\src\pages\free_assistance\free_assistance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */]])
    ], FreeAssistancePage);
    return FreeAssistancePage;
}());

//# sourceMappingURL=free_assistance.js.map

/***/ })

},[238]);
//# sourceMappingURL=main.js.map